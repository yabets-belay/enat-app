import 'package:enat/screens/vaccination.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test vaccination screen renders', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: Vaccination()));

    expect(find.text('ይህ የቀን ማስያ ክትባት መቼ መከተብ እንዳለብሽ ይነግርሻል'), findsOneWidget);
  });

  testWidgets(
      'Test switch button changes description text on vaccination screen',
      (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: Vaccination()));
    expect(find.text('የልጅሽ ውልደት ቀን'), findsOneWidget);

    await tester.tap(find.byType(Switch));
    await tester.pumpAndSettle();
    expect(find.text('የመጀመሪያ ክትባትሽን የወሰድሽበት ቀን'), findsOneWidget);
  });
}
