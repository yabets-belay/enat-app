import 'package:enat/screens/weight_calculator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test weight calculator screen renders',
      (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: WeightCalculator()));

    expect(find.text('የክብደት ማሰቢያ'), findsOneWidget);
  });
}
