import 'package:enat/screens/nutrition.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test child food screen renders', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: Nutrition()));

    expect(find.text('የልጅሽ አመጋገብ አስቢ'), findsOneWidget);
  });

  testWidgets('Test switch button changes description text',
      (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: Nutrition()));
    expect(
        find.text('በቀን ምን ያህል የጡት ወተት ለልጅሽ መስጠት እንደሚገባ ያሰላል'), findsOneWidget);

    await tester.tap(find.byType(Switch));
    await tester.pumpAndSettle();
    expect(
        find.text('በቀን ምን ያህል የዱቄት ወተት ለልጅሽ መስጠት እንደሚገባ ያሰላል'), findsOneWidget);
  });
}
