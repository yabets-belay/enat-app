import 'package:enat/screens/detail_screen.dart';
import 'package:enat/widgets/enat_paragraph.dart';
import 'package:enat/widgets/image_jumbotron.dart';
import 'package:enat/widgets/sub_title_header.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test detail screen render all widgets',
      (WidgetTester tester) async {
    await tester.pumpWidget(const MediaQuery(
        data: MediaQueryData(),
        child: MaterialApp(home: DetailScreen(data: testData))));
    await tester.pumpAndSettle(const Duration(milliseconds: 1000));
    expect(find.byType(ImageJumbotron), findsOneWidget);
    expect(find.byType(EnatParagraph), findsNWidgets(2));
    expect(find.byType(SubTitleHeader), findsOneWidget);
  });
}

const testData = {
  "header": "በእርግዝና ወቅት ሊያጋጥሙሽ የሚችሉ ጤናማ የሰውነት ለውጦች",
  "back": "lanchi",
  "image": 'assets/tabs/lanchi/1.jpg',
  "data": [
    {
      "id": 0,
      "type": 'jumbotron',
      "image": 'test_resources/image.jpg',
    },
    {
      "id": 1,
      "type": "text",
      "text":
          "በእርግዝና ወቅት ሰውነትሽ ብዙና ትልቅ የሚባሉ ለውጦችን ያስተናግዳል፡፡ ብዙዎች ለውጦች ከእርግዝና መፈጠር በኋላ ወዲያው የሚጀምሩና እስከ እርግዝና ፍፃሜ የሚቀጠሉ ናቸው፡፡ ከወሊድና ብሎም ጡት ማጥባት ከተውሽ በኋላ እነዚህ ለውጦች ሙሉ በሙሉ በሚባል ደረጃ ተመልሰው ይጠፋሉ፡፡"
    },
    {"id": 2, "type": "subtitle", "text": "ማህጸን"},
    {
      "id": 3,
      "type": "text",
      "text":
          "ከእርግዝና በፊት ማህጸንሽ ወደ 70 ግራም የሚመዝን ስጋማ አካል ሲሆን ይዘቱም 10 ሚሊ ብቻ ነው፡፡ በእርግዝናሽ ወቅት መጠኑ በእጅጉ ጨምሮ ክብደቱ ወደ 1000 ግራም ይዘቱም ወደ 5 ሊትር በአማካይ ይሆናል፡፡ 9ወር ሲሞላሽ ወደማህጸንሽ ከ450-650 ሚ-ሊ ደም በደቂቃ ይደርሳል፡፡"
    },
    {"id": 4, "type": "title", "text": "ጡት"},
    {
      "id": 5,
      "type": "text",
      "text":
          "የእርግዝናሽ መጀመሪያ ሳምንታት ትንሽ የመጠዝጠዝ እና ሲነካ ህመም ሊኖረው ይችላል፡፡ ከ2-3 ወር በኋላ በጡትሽ ላይ ቀጭን የደም ስሮች የሚታዩ ሲሆን የጡትሽ ጫፍም በመጠኑ እየጨመረ መልኩም እየጠቆረ እንዲሁም ቀጥ ብሎ የመቆም ለውጦች ይታዩበታል፡፡ ከጥቂት የእርግዝና ወራት በኋላ ከጡትሽ ቢጫ መሰል ወፍራም ፈሳሽ የሚወጣ ሲሆን ይህም ጤናማ ሂደት ነው፡፡ ከእርግዝናሽ በፊት ያለ የጡትሽ መጠን እና ከወሊድ በኋላ የሚመረተው ወተት መጠን ቀጥታ ግንኙነት የላቸውም፡፡ ማለትም ትንሽ ጡት ቢኖርሽ ብዙ ወተት አይኖርሽም ማለት አይደለም፡፡"
    },
    {"id": 6, "type": "subtitle", "text": "የደም መጠን"},
    {
      "id": 8,
      "type": "bulletin",
      "list": [
        {"id": 0, "text": "ለማህጸንሽ አስፈላጊውን ደም ለማድረስ"},
        {"id": 1, "text": "በምጥና ወሊድ ወቅት ከሚፈጠር የደም መድማት ለመቋቋም"}
      ]
    },
    {"id": 11, "type": "banner", "bannerid": "Lanchi001"},
  ]
};
