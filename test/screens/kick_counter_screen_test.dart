import 'package:enat/screens/kick_counter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test kick counter screen renders', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: KickCounter()));

    expect(find.text('የልጅዎን እንቅስቃሴ ይቆጣጠሩ'), findsOneWidget);
  });
}
