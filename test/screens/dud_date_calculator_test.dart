import 'package:enat/Abushakir/abushakir.dart';
import 'package:enat/screens/due_date_calculator.dart';
import 'package:enat/utility.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  testWidgets('Test due date screen renders', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: DueDateCalculator()));

    expect(
        find.text('ይህ የመውለጃ ቀንሽን እና ምን ያህል ሳምንት እንደቀረሽ ለማወቅ የሚረዳሽ የቀን ማስያ ነው።'),
        findsOneWidget);
  });

  testWidgets('Test button stores the selected date',
      (WidgetTester tester) async {
    var birthday = EtDateTime.now().add(const Duration(days: 55));
    SharedPreferences.setMockInitialValues({'DUE_DATE': birthday.toString()});

    await tester.pumpWidget(const MaterialApp(home: DueDateCalculator()));
    await tester.tap(find.byKey(const Key('birthdayBtn')));
    await tester.pump();

    var storedDate = await getDueDate();
    var today = EtDateTime.now();

    expect(storedDate.day, equals(today.day));
    expect(storedDate.month, equals(today.month));
    expect(storedDate.year, equals(today.year));
  });
}
