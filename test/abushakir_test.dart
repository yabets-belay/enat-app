import 'package:enat/Abushakir/abushakir.dart';
import 'package:test/test.dart';

void main() {
  group('Parameterized Constructors :', () {
    late EtDateTime ec;

    setUp(() {
      ec = EtDateTime(year: 2012, month: 07, day: 07);
    });

    test('Testing Year on Parameterized Constructor', () {
      expect(ec.year, 2012);
    });

    test('Testing Month on Parameterized Constructor', () {
      expect(ec.month, 07);
    });

    test('Testing Day on Parameterized Constructor', () {
      expect(ec.day, 07);
    });

    test('Testing Date on Parameterized Constructor', () {
      expect(ec.dayGeez, "፯");
    });
  });

  group('Parameterized Constructors (year only) :', () {
    late EtDateTime ec;

    setUp(() {
      ec = EtDateTime(year: 2010);
    });

    test('Testing Year on Parameterized Constructor', () {
      expect(ec.year, 2010);
    });

    test('Testing Month on Parameterized Constructor', () {
      expect(ec.month, 01);
    });

    test('Testing Day on Parameterized Constructor', () {
      expect(ec.day, 01);
    });
  });

  group('Named Constructors (.parse) :', () {
    late EtDateTime ec;

    setUp(() {
      ec = EtDateTime.parse("2012-07-07 15:12:17.500");
    });

    test('Should Print the parsed Date and Time', () {
      expect(ec.toString(), "2012-07-07 15:12:17.500");
    });

    test('Testing Year on Named Constructor', () {
      expect(ec.year, 2012);
    });

    test('Testing Month on Named Constructor', () {
      expect(ec.month, 07);
    });

    test('Testing Day on Named Constructor', () {
      expect(ec.day, 07);
    });

    test('Testing Date with Named Constructor', () {
      expect(ec.dayGeez, "፯");
    });

    test('Testing Hour on Named Constructor', () {
      expect(ec.hour, 15);
    });

    test('Testing Minute on Named Constructor', () {
      expect(ec.minute, 12);
    });

    test('Testing Second on Named Constructor', () {
      expect(ec.second, 17);
    });

    test('Testing Millisecond on Named Constructor', () {
      expect(ec.millisecond, 500);
    });
  });

  group('Named Constructors (.now) :', () {
    late EtDateTime ec;

    setUp(() {
      ec = EtDateTime.now();
    });
    test('Testing Minute on .now() Named Constructor', () {
      expect(ec.minute, DateTime.now().minute);
    });

    test('Testing Second on .now() Named Constructor', () {
      expect(ec.second, DateTime.now().second);
    });
  });

  group('Named Constructors (.fromMillisecondsSinceEpoch) :', () {
    late EtDateTime ec;

    // 1585731446021 == 2012-07-23 08:57:26.021
    setUp(() {
      ec = EtDateTime.fromMillisecondsSinceEpoch(1585731446021);
    });

    test('Should Print the parsed Date and Time', () {
      expect(ec.toString(), "2012-07-23 08:57:26.021");
    });

    test('Testing Year on Named Constructor', () {
      expect(ec.year, 2012);
    });

    test('Testing Month on Named Constructor', () {
      expect(ec.month, 07);
    });

    test('Testing Day on Named Constructor', () {
      expect(ec.day, 23);
    });

    test('Testing Date with Named Constructor', () {
      expect(ec.dayGeez, "፳፫");
    });

    test('Testing Hour on Named Constructor', () {
      expect(ec.hour, 08);
    });

    test('Testing Minute on Named Constructor', () {
      expect(ec.minute, 57);
    });

    test('Testing Second on Named Constructor', () {
      expect(ec.second, 26);
    });

    test('Testing Millisecond on Named Constructor', () {
      expect(ec.millisecond, 021);
    });
  });

  group('Named Constructors (.now) :', () {
    late EtDateTime ec;

    setUp(() {
      ec = EtDateTime.now();
    });
    test('Testing Minute on .now() Named Constructor', () {
      expect(ec.minute, DateTime.now().minute);
    });

    test('Testing Second on .now() Named Constructor', () {
      expect(ec.second, DateTime.now().second);
    });
  });

  group('Testing functions', () {
    late EtDateTime ec;
    setUp(() {
      // TODO: check if all constructors return same epoch.
//      ec = EtDatetime.now();
      ec = EtDateTime(year: 2012);
    });
    // 1585742246021 == 2012-07-23 11:57:26.021
    test('Testing isAfter()', () {
      expect(ec.isAfter(EtDateTime(year: 2011)), true);
    });
    test('Testing isBefore()', () {
      expect(ec.isBefore(EtDateTime(year: 2080)), true);
    });
    test('Testing isAfter()', () {
      expect(
          ec.isAtSameMomentAs(EtDateTime(
              year: ec.year,
              month: ec.month,
              day: ec.day,
              hour: ec.hour,
              minute: ec.minute,
              second: ec.second,
              millisecond: ec.millisecond)),
          true);
    });
  });
}
