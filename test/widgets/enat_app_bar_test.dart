import 'package:enat/widgets/enat_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test app bar renders', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(
      home: Scaffold(
        appBar: EnatAppBar(),
      ),
    ));

    expect(find.text('እናት'), findsOneWidget);
  });
}
