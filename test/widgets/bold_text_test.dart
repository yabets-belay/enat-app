import 'package:enat/widgets/bold_text.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test BoldText widget', (WidgetTester tester) async {
    const text = 'የሚፈልጓቸውን ልጆች';

    await tester.pumpWidget(const BoldText(text: text));

    expect(find.text(text), findsOneWidget);
  });
}
