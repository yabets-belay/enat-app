import 'package:enat/widgets/baby_name.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test baby name list renders', (WidgetTester tester) async {
    const data = {
      "male": [
        {"id": "1", "name": "male name", "meaning": "meaning of male name"}
      ],
      "female": [
        {"id": "2", "name": "female name", "meaning": "meaning of female name"}
      ]
    };

    await tester.pumpWidget(MaterialApp(
        home:
            Scaffold(body: ListView(children: const [BabyName(data: data)]))));

    expect(find.text('የወንድ'), findsOneWidget);
    expect(find.text('የሴት'), findsOneWidget);
    expect(find.text("male name"), findsOneWidget);
    expect(find.text("female name"), findsOneWidget);
  });
}
