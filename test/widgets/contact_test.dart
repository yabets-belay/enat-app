import 'package:enat/widgets/contact.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test Contact widget', (WidgetTester tester) async {
    const name = 'name';
    const phone = '123456789';

    await tester.pumpWidget(const MaterialApp(
        home: Scaffold(body: Contact(name: name, phoneNumber: phone))));

    expect(find.text(name), findsOneWidget);
    expect(find.text(phone), findsOneWidget);
  });
}
