import 'package:enat/widgets/enat_table.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test Table renders 2 x 3 widget', (WidgetTester tester) async {
    final List<List<String>> data = [
      ['a', 'b'],
      ['c', 'd'],
      ['e', 'f']
    ];
    await tester.pumpWidget(MaterialApp(home: EnatTable(data: data)));

    expect(find.byType(Table), findsOneWidget);
  });

  testWidgets('Test Table renders 4 x 2 widget', (WidgetTester tester) async {
    final List<List<String>> data = [
      ['a', 'b', 'c', 'd'],
      ['e', 'f', 'g', 'h']
    ];
    await tester.pumpWidget(MaterialApp(home: EnatTable(data: data)));

    expect(find.byType(Table), findsOneWidget);
  });
}
