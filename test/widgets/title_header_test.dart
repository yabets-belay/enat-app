import 'package:enat/widgets/title_header.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test TitleHeader widget render title',
      (WidgetTester tester) async {
    const titleInput = 'Heading 1 title';
    await tester.pumpWidget(const TitleHeader(title: titleInput));

    final titleFinder = find.text(titleInput);
    expect(titleFinder, findsOneWidget);
  });
}
