import 'package:enat/widgets/image_jumbotron.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test ImageJumbotron renders image', (WidgetTester tester) async {
    const imageSource = 'test_resources/image.jpg';

    Widget testWidget = const MediaQuery(
        data: MediaQueryData(),
        child: ImageJumbotron(imgLocation: imageSource));

    await tester.pumpWidget(testWidget);

    expect(find.byType(Image), findsOneWidget);
  });
}
