import 'package:enat/widgets/enat_paragraph.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test Paragraph widget', (WidgetTester tester) async {
    const paragraphBody =
        'የሚፈልጓቸውን ልጆች መጠን ለወለዱና ተጨማሪ ልጅ ለመውለድ ለማያስቡ ጥንዶች ይህ ዘዴ ጠቃሚ ነው፡፡ ቀለል ባለ የቀዶ ህክምና እርግዝናን ለዘለቄታው መከላከል የሚያስችል ነው፡፡ ቀዶ ጥገናው በወንዱም በሴቷም ሊደረግ የሚችል ሲሆን በወንዱ የሚከናወነው ቀዶ ጥገና የወንዱ የዘር ፍሬ የሚተላለፍበትን ቱቦ በመዝጋት ነው፡፡ በሴቷ ደግሞ እንቁላል ወደ ማህፀን የሚተላለፍበትን ቱቦ በመቛጠር ነው፡፡';

    await tester.pumpWidget(const EnatParagraph(body: paragraphBody));

    expect(find.text(paragraphBody), findsOneWidget);
  });
}
