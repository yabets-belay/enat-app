import 'package:enat/widgets/enat_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test Enat Button Widget renders', (WidgetTester tester) async {
    const text = 'Done';

    await tester.pumpWidget(
        MaterialApp(home: EnatButton(text: text, onPressed: () {})));

    expect(find.text(text), findsOneWidget);
  });
}
