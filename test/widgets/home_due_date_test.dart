import 'package:enat/Abushakir/abushakir.dart';
import 'package:enat/widgets/home_due_date.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  testWidgets('Test birth date by stating state', (WidgetTester tester) async {
    var birthday = EtDateTime.now().add(const Duration(days: 55));
    SharedPreferences.setMockInitialValues({'DUE_DATE': birthday.toString()});

    await tester.pumpWidget(const MaterialApp(
      home: Scaffold(
        body: HomeDueDate(),
      ),
    ));
    await tester.pumpAndSettle();

    expect(find.byType(Text), findsNWidgets(3));

    expect(find.text('አሁን ያለሽበት ሳምንት 33ኛ ሳምንት'), findsOneWidget);
    expect(find.text('ልጅሽን ለመውለድ የቀረሽ 7 ሳምንት ከ6 ቀን'), findsOneWidget);
    expect(
        find.text(
            'ቀኑ ${birthday.monthGeez} ${birthday.day} ፤ ${birthday.year}'),
        findsOneWidget);
  });

  testWidgets('Test birth date for born baby', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(
      home: Scaffold(
        body: HomeDueDate(),
      ),
    ));

    expect(find.text('አሁን እናት ነሽ'), findsOneWidget);
  });
}
