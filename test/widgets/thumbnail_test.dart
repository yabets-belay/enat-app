import 'package:enat/widgets/thumbnail.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  const imageSource = 'test_resources/image.jpg';
  const secondarySource = 'test_resources/small_image.png';
  testWidgets('Test primary image renders', (WidgetTester tester) async {
    Widget testWidget =
        const MaterialApp(home: Thumbnail(primaryImage: imageSource));

    await tester.pumpWidget(testWidget);

    expect(find.byType(Image), findsOneWidget);
  });

  testWidgets('Test primary image and second image renders',
      (WidgetTester tester) async {
    Widget testWidget = const MaterialApp(
        home: Thumbnail(
      primaryImage: imageSource,
      secondaryImage: secondarySource,
    ));

    await tester.pumpWidget(testWidget);

    expect(find.byType(Image), findsNWidgets(2));
  });
}
