import 'package:enat/widgets/sub_title_header.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test SubTitleHeader', (WidgetTester tester) async {
    const titleInput = 'Sub heading title';

    await tester.pumpWidget(const SubTitleHeader(title: titleInput));

    expect(find.text(titleInput), findsOneWidget);
  });
}
