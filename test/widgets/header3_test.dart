import 'package:enat/widgets/header3.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test header3 readers', (WidgetTester tester) async {
    const header = 'header text';

    await tester.pumpWidget(const MaterialApp(home: Header3(title: header)));

    expect(find.text(header), findsOneWidget);
  });
}
