import 'package:enat/widgets/kick_progress_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test Kick Progress Indicator renders',
      (WidgetTester tester) async {
    await tester.pumpWidget(
        const MaterialApp(home: KickProgressIndicator(currentCount: 8)));

    expect(find.byType(Container), findsNWidgets(10));
  });
}
