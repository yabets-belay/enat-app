import 'package:enat/widgets/card_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test card is rendered', (WidgetTester tester) async {
    const data = {
      'image': 'test_resources/image.jpg',
      'header': 'Card item title'
    };

    await tester.pumpWidget(const MaterialApp(home: CardItem(data: data)));

    expect(find.byType(Image), findsOneWidget);
    expect(find.text('Card item title'), findsOneWidget);
  });
}
