import 'package:enat/widgets/bullet_item.dart';
import 'package:enat/widgets/bulletin.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test bulletin list', (WidgetTester tester) async {
    const list = [
      {
        "id": 0,
        "text":
            "ልጅ ከወለድሽ ገና 48 ሰአት ካልሞላሽ ፦ በማህጸን ውስጥ የሚቀመጥ (አይዩዲ) ፣ በእጅ ውስጥ የሚቀመጥ (ኢምፕላንት) ወይም ባለነጠላ ንጥረ ነገር እንክብል "
      },
      {
        "id": 1,
        "text":
            "ልጅ ከወለድሽ ከ 48 ሰአት እስከ አራት ወርሽ ከሆነ ፦ በእጅ ውስጥ የሚቀመጥ (ኢምፕላንት) ወይም ባለነጠላ ንጥረ ነገር እንክብል "
      },
      {
        "id": 2,
        "text":
            "ልጅ ከወለድሽ ከ አራት ሳምንት እስከ ስድስት ሳምንት ከሆነ ፦ በማህጸን ውስጥ የሚቀመጥ (አይዩዲ)፣ በእጅ ውስጥ የሚቀመጥ (ኢምፕላንት) ወይም ባለነጠላ ንጥረ ነገር እንክብል "
      },
      {
        "id": 3,
        "text":
            "ልጅ ከወለድሽ ከ 6 ሳምንት - 6 ወር፦ በማህጸን ውስጥ የሚቀመጥ አዩዲ ፣ በእጅ ውስጥ የሚቀመጥ ኢምፕላንት ወይም ባለነጠላ ንጥረ ነገር እንክብል ወይም በተለምዶ መርፌ "
      }
    ];

    await tester.pumpWidget(const Bulletin(data: list));

    expect(
        tester.widgetList(find.byType(BulletItem)).length, equals(list.length));

    expect(find.text(list[0]['text'].toString()), findsOneWidget);
  });
}
