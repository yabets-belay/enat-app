import 'package:enat/widgets/fullscreen_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test FullScreen image widget', (WidgetTester tester) async {
    const imageSource = 'test_resources/image.jpg';

    await tester.pumpWidget(const MaterialApp(
        home: FullscreenImage(
      imageSource: imageSource,
    )));

    expect(find.byType(Image), findsOneWidget);
  });
}
