import 'package:flutter/material.dart';

class FullscreenImage extends StatelessWidget {
  final String imageSource;
  const FullscreenImage({Key? key, required this.imageSource})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 0.75,
      child: Image(
        image: AssetImage(imageSource),
        fit: BoxFit.fitHeight,
      ),
    );
  }
}
