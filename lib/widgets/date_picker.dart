import 'dart:math';

import 'package:enat/Abushakir/abushakir.dart';
import 'package:flutter/material.dart';

import '../utility.dart';

class DatePicker extends StatefulWidget {
  final EtDateTime etDate;
  final Function setDate;

  const DatePicker({Key? key, required this.etDate, required this.setDate})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _DatePicker();
}

class _DatePicker extends State<DatePicker> {
  final month = [
    'መስከረም',
    'ጥቅምት',
    'ህዳር',
    'ታህሳስ',
    'ጥር',
    'የካቲት',
    'መጋቢት',
    'ሚያዚያ',
    'ግንቦት',
    'ሰኔ',
    'ሐምሌ',
    'ነሐሴ',
    'ጳጉሜ'
  ];
  final numOfYears = 3;
  late int selectedMonth;
  late int selectedDay;
  late int selectedYear;
  late int endOfMonth;
  late final int lastYear;

  @override
  void initState() {
    super.initState();
    selectedMonth = widget.etDate.month - 1;
    selectedDay = widget.etDate.day;
    selectedYear = widget.etDate.year;
    endOfMonth = _getEndOfMonth();
    lastYear = getLastYear();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _buildDropdownButton(context, selectedMonth, _setMonth,
            _generateMonthDropDownMenuItem()),
        _buildDropdownButton(
            context, selectedDay, _setDay, _generateDayDropDownMenuItem()),
        _buildDropdownButton(
            context, selectedYear, _setYear, _generateYearDropDownMenuItem())
      ],
    );
  }

  _getEndOfMonth() {
    if (selectedMonth == 12) {
      return selectedYear % 4 == 3 ? 6 : 5;
    }
    return 30;
  }

  List<DropdownMenuItem<int>> _generateYearDropDownMenuItem() {
    return List.generate(numOfYears, (index) => index + lastYear)
        .map<DropdownMenuItem<int>>((int value) {
      return DropdownMenuItem<int>(
        value: value,
        child: Text(value.toString()),
      );
    }).toList();
  }

  void _setYear(int? newValue) {
    setState(() {
      selectedYear = newValue!;
      endOfMonth = _getEndOfMonth();
      selectedDay = min(selectedDay, endOfMonth);
    });
    _handleEtDateSet();
  }

  List<DropdownMenuItem<int>> _generateDayDropDownMenuItem() {
    return List.generate(endOfMonth, (index) => index + 1)
        .map<DropdownMenuItem<int>>((int value) {
      return DropdownMenuItem<int>(
        value: value,
        child: Text(value.toString()),
      );
    }).toList();
  }

  void _setDay(int? newValue) {
    setState(() {
      selectedDay = newValue!;
    });
    _handleEtDateSet();
  }

  Widget _buildDropdownButton(
      BuildContext context, var selectedValue, var onChange, var items) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: DropdownButton<int>(
        value: selectedValue,
        icon: const Icon(Icons.arrow_drop_down),
        style: Theme.of(context).textTheme.overline,
        onChanged: onChange,
        items: items,
      ),
    );
  }

  void _setMonth(int? newValue) {
    setState(() {
      selectedMonth = newValue!;
      endOfMonth = _getEndOfMonth();
      selectedDay = min(selectedDay, endOfMonth);
    });
    _handleEtDateSet();
  }

  void _handleEtDateSet() {
    widget.setDate(EtDateTime(
        year: selectedYear, month: selectedMonth + 1, day: selectedDay));
  }

  _generateMonthDropDownMenuItem() {
    List<DropdownMenuItem<int>> result = [];
    for (int i = 0; i < month.length; i++) {
      result.add(DropdownMenuItem<int>(value: i, child: Text(month[i])));
    }
    return result;
  }
}
