import 'package:flutter/material.dart';

class BulletItem extends StatelessWidget {
  final String text;

  const BulletItem({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8, right: 8),
      child: Row(
        textDirection: TextDirection.ltr,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '\u2022',
            style: TextStyle(
              color: Theme.of(context).primaryColor,
            ),
            textAlign: TextAlign.start,
            textDirection: TextDirection.ltr,
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(left: 4),
              child: Text(
                text,
                style: Theme.of(context).textTheme.bodyText1,
                softWrap: true,
                textDirection: TextDirection.ltr,
              ),
            ),
          )
        ],
      ),
    );
  }
}
