import 'package:flutter/material.dart';

import '../utility.dart';

class Thumbnail extends StatelessWidget {
  final String primaryImage;
  final String? secondaryImage;
  const Thumbnail({Key? key, required this.primaryImage, this.secondaryImage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Image(
            width: MediaQuery.of(context).size.width * 0.7,
            image: AssetImage(primaryImage)),
        secondaryImage != null
            ? Image(
                width: MediaQuery.of(context).size.width * 0.2,
                height: 60,
                image: AssetImage(secondaryImage!),
              )
            : emptyWidget(),
      ],
    );
  }
}
