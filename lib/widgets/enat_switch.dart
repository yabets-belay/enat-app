import 'package:flutter/material.dart';

class EnatSwitch extends StatelessWidget {
  final String leftLabel;
  final String rightLabel;
  final bool value;
  final ValueChanged<bool> onChange;
  const EnatSwitch(
      {Key? key,
      required this.leftLabel,
      required this.rightLabel,
      required this.value,
      required this.onChange})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(leftLabel),
        Switch(
          activeColor: Colors.grey,
          activeTrackColor: Colors.grey,
          inactiveThumbColor: Colors.grey,
          inactiveTrackColor: Colors.grey,
          value: value,
          onChanged: onChange,
        ),
        Text(rightLabel),
      ],
    );
  }
}
