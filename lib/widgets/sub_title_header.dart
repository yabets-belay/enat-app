import 'package:flutter/material.dart';

class SubTitleHeader extends StatelessWidget {
  final String title;
  const SubTitleHeader({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      textDirection: TextDirection.ltr,
      children: [
        Expanded(
            child: Container(
          margin: const EdgeInsets.only(bottom: 8),
          padding: const EdgeInsets.all(8),
          child: Text(
            title,
            style: Theme.of(context).textTheme.headline2,
            textDirection: TextDirection.ltr,
          ),
        ))
      ],
    );
  }
}
