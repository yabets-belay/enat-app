import 'package:enat/widgets/enat_paragraph.dart';
import 'package:flutter/material.dart';

class EnatTable extends StatelessWidget {
  final List<List<String>> data;
  const EnatTable({
    Key? key,
    required this.data,
    body,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Table(
      border: TableBorder.symmetric(
          inside: const BorderSide(width: 2, color: Colors.white)),
      children: _buildTableRows(context),
    );
  }

  _buildTableRows(BuildContext context) {
    var darkRow =
        BoxDecoration(color: Theme.of(context).colorScheme.onTertiary);
    var lightRow =
        BoxDecoration(color: Theme.of(context).colorScheme.secondary);

    return List<TableRow>.generate(data.length, (index) {
      return TableRow(decoration: index.isEven ? darkRow : lightRow, children: [
        ...data[index].map((col) {
          return EnatParagraph(body: col);
        })
      ]);
    });
  }
}
