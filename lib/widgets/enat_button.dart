import 'package:enat/utility.dart';
import 'package:flutter/material.dart';

class EnatButton extends StatelessWidget {
  final Function onPressed;
  final String text;
  final String? icon;
  const EnatButton(
      {Key? key, required this.text, required this.onPressed, this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ButtonStyle buttonStyle = ElevatedButton.styleFrom(
      primary: Theme.of(context).colorScheme.primary,
      onPrimary: Theme.of(context).colorScheme.onPrimary,
      padding: const EdgeInsets.symmetric(horizontal: 16),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(2)),
      ),
    );
    return ElevatedButton(
        style: buttonStyle,
        onPressed: () => onPressed(),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            icon != null ? Image(image: AssetImage(icon!)) : emptyWidget(),
            Text(text),
          ],
        ));
  }
}
