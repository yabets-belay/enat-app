import 'package:enat/widgets/bullet_item.dart';
import 'package:flutter/material.dart';

class Bulletin extends StatelessWidget {
  /// data = [ {"id": 0, "text": "body"}];
  final List data;

  const Bulletin({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ...data.map((item) => Padding(
            padding: const EdgeInsets.only(bottom: 4),
            child: BulletItem(text: item['text'].toString())))
      ],
    );
  }
}
