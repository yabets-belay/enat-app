import 'package:flutter/material.dart';

class Header3 extends StatelessWidget {
  final String title;
  const Header3({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        title,
        style: Theme.of(context).textTheme.headline3,
      ),
    );
  }
}
