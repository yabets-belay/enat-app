import 'package:flutter/material.dart';

import 'enat_button.dart';
import 'enat_paragraph.dart';

class CancelButton extends StatelessWidget {
  final String contentBody;
  final String successMessage;
  final Function cancelAction;

  const CancelButton(
      {Key? key,
      required this.contentBody,
      required this.successMessage,
      required this.cancelAction})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: EnatButton(
          text: 'ሰርዝ',
          onPressed: () {
            return showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: const Text('ማስጠንቀቂያ'),
                    content: EnatParagraph(body: contentBody),
                    actions: [
                      TextButton(
                          child: const Text('ሰርዝ'),
                          onPressed: () {
                            Navigator.of(context).pop();
                            cancelAction();
                            ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(content: Text(successMessage)));
                          }),
                      TextButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: const Text('ይቅር'))
                    ],
                  );
                });
          }),
    );
  }
}
