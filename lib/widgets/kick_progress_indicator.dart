import 'package:flutter/material.dart';

class KickProgressIndicator extends StatelessWidget {
  final int currentCount;
  const KickProgressIndicator({Key? key, required this.currentCount})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List<Container>.generate(
          10, (index) => _buildBox(index < currentCount, context)),
    );
  }

  _buildBox(bool active, BuildContext context) {
    return Container(
      width: 24,
      height: 8,
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(4.0)),
        color: active
            ? Theme.of(context).primaryColor
            : const Color.fromRGBO(210, 210, 212, 1),
      ),
      margin: const EdgeInsets.all(4.0),
    );
  }
}
