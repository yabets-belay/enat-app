import 'package:flutter/material.dart';

class ImageJumbotron extends StatelessWidget {
  final String imgLocation;
  const ImageJumbotron({Key? key, required this.imgLocation}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      textDirection: TextDirection.ltr,
      children: [
        Expanded(
            child: Image(
          image: AssetImage(imgLocation),
          fit: BoxFit.cover,
          height: (217 * MediaQuery.of(context).size.width) / 375,
        ))
      ],
    );
  }
}
