import 'package:flutter/material.dart';

import '../Abushakir/abushakir.dart';
import '../utility.dart';

class HomeDueDate extends StatefulWidget {
  const HomeDueDate({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HomeDueDate();
}

class _HomeDueDate extends State<HomeDueDate> {
  final imageSource = 'assets/home.jpg';
  var isBorn = true;
  var remainingDays = 0;
  var remainingWeeks = 0;
  var birthday = EtDateTime.now();

  @override
  void initState() {
    super.initState();
    _getDueDate();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image(
          image: AssetImage(imageSource),
          fit: BoxFit.cover,
          height: MediaQuery.of(context).size.height,
        ),
        Positioned(
            bottom: 0,
            child: isBorn
                ? _getBornWidget(context)
                : _getRemainingDaysWidget(context))
      ],
    );
  }

  void _getDueDate() async {
    final storedDate = await getDueDate();
    _calculateDueDate(storedDate);
  }

  void _calculateDueDate(EtDateTime date) {
    final today = EtDateTime.now();
    if (date.isBefore(today)) {
      setState(() {
        isBorn = true;
      });
    } else {
      var duration = date.difference(today);
      var days = duration.inDays;
      setState(() {
        remainingDays = days % 7;
        remainingWeeks = days ~/ 7;
        isBorn = false;
        birthday = date;
      });
    }
  }

  Widget _getBornWidget(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.all(8),
      color: Theme.of(context).colorScheme.onTertiary,
      child: Center(
        child: Text(
          'አሁን እናት ነሽ',
          style: Theme.of(context).textTheme.subtitle2,
        ),
      ),
    );
  }

  Widget _getRemainingDaysWidget(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.all(8),
      color: Theme.of(context).colorScheme.onTertiary,
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Center(
          child: Text(
            'አሁን ያለሽበት ሳምንት ${40 - remainingWeeks}ኛ ሳምንት',
            style: Theme.of(context).textTheme.caption,
          ),
        ),
        Center(
          child: Text(
            'ልጅሽን ለመውለድ የቀረሽ $remainingWeeks ሳምንት ከ$remainingDays ቀን',
            style: Theme.of(context).textTheme.caption,
          ),
        ),
        Center(
          child: Text(
            'ቀኑ ${birthday.monthGeez} ${birthday.day} ፤ ${birthday.year}',
            style: Theme.of(context).textTheme.caption,
          ),
        ),
      ]),
    );
  }
}
