import 'package:flutter/material.dart';

class TitleHeader extends StatelessWidget {
  final String title;

  const TitleHeader({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      textDirection: TextDirection.ltr,
      children: [
        Expanded(
          child: Container(
            margin: const EdgeInsets.only(bottom: 8),
            padding: const EdgeInsets.all(8),
            color: Theme.of(context).colorScheme.secondary,
            child: Text(
              title,
              style: Theme.of(context).textTheme.headline1,
              textDirection: TextDirection.ltr,
            ),
          ),
        ),
      ],
    );
  }
}
