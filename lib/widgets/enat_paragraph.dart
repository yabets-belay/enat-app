import 'package:flutter/material.dart';

class EnatParagraph extends StatelessWidget {
  final String body;
  const EnatParagraph({Key? key, required this.body}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Text(body,
          softWrap: true,
          style: Theme.of(context).textTheme.bodyText1,
          textDirection: TextDirection.ltr),
    );
  }
}
