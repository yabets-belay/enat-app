import 'package:enat/screens/detail_tab_screen.dart';
import 'package:flutter/material.dart';

import '../screens/detail_screen.dart';

class CardItem extends StatelessWidget {
  /// data will have the following structure
  /*
  {
      "header" // card title
      "image" // location to the card image
      "data": [ // the detail screen content is stored as list of widgets
        {"id", "type", "value"} // detail screen widgets
       ]
     }
   */
  final Map<String, dynamic> data;

  const CardItem({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => _getDetailScreen()),
      ),
      child: Card(
        color: Theme.of(context).colorScheme.tertiary,
        child: Column(
          children: [
            Image(
              image: AssetImage(data['image']),
              width: double.infinity,
              fit: BoxFit.fitWidth,
            ),
            Padding(
              padding: const EdgeInsets.all(4),
              child: Text(
                data['header'],
                style: Theme.of(context).textTheme.subtitle1,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _getDetailScreen() {
    if (data.containsKey('tabs')) {
      return DetailTabScreen(data: data);
    }
    return DetailScreen(data: data);
  }
}
