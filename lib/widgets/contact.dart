import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class Contact extends StatelessWidget {
  final String name;
  final String phoneNumber;
  const Contact({Key? key, required this.name, required this.phoneNumber})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: const Icon(Icons.account_circle_rounded),
      title: Text(
        name,
        style: Theme.of(context).textTheme.headline1,
      ),
      subtitle: Text(
        phoneNumber,
        style: Theme.of(context).textTheme.bodyText1,
      ),
      trailing: const Icon(Icons.phone),
      onTap: () => _makePhoneCall(phoneNumber),
    );
  }

  Future<void> _makePhoneCall(String phoneNumber) async {
    final Uri launchUri = Uri(
      scheme: 'tel',
      path: phoneNumber,
    );
    await launchUrl(launchUri);
  }
}
