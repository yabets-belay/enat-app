import 'package:enat/utility.dart';
import 'package:flutter/material.dart';

const babyName = "BABY_NAME";

class BabyName extends StatefulWidget {
  /// data have the following structure
  /*
  {
    "male": [
      {
        "id": 0,
        "name": "አዲስ",
        "meaning": "አዲስ"
      }, ... more male names
    ],
    "female: [
      {
        "id": 0,
        "name": "አበባ",
        "meaning": "አበባ"
      }, ... more female names
    ],
  }

   */
  final Map data;
  const BabyName({Key? key, required this.data}) : super(key: key);

  @override
  State<BabyName> createState() => _BabyNameState();
}

class _BabyNameState extends State<BabyName> {
  Set<String> favorites = {};
  @override
  void initState() {
    super.initState();
    _getFavoriteNames();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _getNameList(context, "የወንድ", widget.data['male']),
        _getNameList(context, "የሴት", widget.data['female'])
      ],
    );
  }

  _getNameList(BuildContext context, String title, List names) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Column(
          children: [
            Text(
              title,
              style: Theme.of(context).textTheme.headline1,
            ),
            ...names.map((item) => _getNameRow(context, item))
          ],
        ),
      ),
    );
  }

  _getNameRow(BuildContext context, Map item) {
    String id = item["id"];
    bool isFav = favorites.contains(id);
    return ListTile(
      leading: isFav
          ? const Icon(Icons.favorite)
          : const Icon(Icons.favorite_border),
      title: Text(
        item['name'],
        style: Theme.of(context).textTheme.headline1,
      ),
      subtitle:
          Text(item['meaning'], style: Theme.of(context).textTheme.bodyText1),
      dense: true,
      onTap: () => _toggleFavorite(id, isFav),
    );
  }

  _getFavoriteNames() async {
    final names = await getBabyNames(babyName);
    setState(() {
      favorites = names;
    });
  }

  _toggleFavorite(String id, bool isFav) {
    final names = Set.of(favorites);
    if (isFav) {
      names.remove(id);
    } else {
      names.add(id);
    }
    saveBabyNames(babyName, names);
    setState(() {
      favorites = names;
    });
  }
}
