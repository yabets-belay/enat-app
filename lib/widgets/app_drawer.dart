import 'package:enat/screens/about_us.dart';
import 'package:enat/screens/due_date_calculator.dart';
import 'package:enat/screens/kick_counter.dart';
import 'package:enat/screens/nutrition.dart';
import 'package:enat/screens/symptoms.dart';
import 'package:enat/screens/vaccination.dart';
import 'package:enat/screens/weight_calculator.dart';
import 'package:enat/widgets/enat_paragraph.dart';
import 'package:flutter/material.dart';

import '../screens/home_screen.dart';

const List<Map<String, String>> data = [
  {
    "name": "የመውለጃ ቀን",
    "route": HomeScreen.routeName,
    "icon": "assets/menu/dueDate.png"
  },
  {
    "name": "የመውለጃ ቀን አስብ",
    "route": DueDateCalculator.routeName,
    "icon": "assets/menu/dueDateCalculator.png"
  },
  {
    "name": "የልጅሽ አመጋገብ",
    "route": Nutrition.routeName,
    "icon": "assets/menu/feed.png",
  },
  {
    "name": "የልጅሽ እንቅስቃሴ መከታተያ",
    "route": KickCounter.routeName,
    "icon": "assets/menu/feet.png",
  },
  {
    "name": "ክብደት አስብ",
    "route": WeightCalculator.routeName,
    "icon": "assets/menu/kilo.png",
  },
  {
    "name": "ክትባት",
    "route": Vaccination.routeName,
    "icon": "assets/menu/vaccination.png"
  },
  {
    "name": "ምልክቶች",
    "route": Symptoms.routeName,
    "icon": "assets/menu/symptoms.png"
  },
  // TODO on version 2
  // {"name": "ማስታወሻ", "route": "Notebook", "icon": "assets/notebook.png"},
  // {"name": "የሀኪም ቀጠሮ", "route": "Reminder", "icon": "assets/reminder.png"},
  // {"name": "የህጻናት ሙዚቃ", "route": "BabyMusic", "icon": "assets/music.png"},
  // {
  //   "name": "ማዕከለ ስዕላት",
  //   "route": "Gallery",
  //   "icon": "assets/pictureGallery.png"
  // },
  {
    "name": "ስለ እኛ",
    "route": AboutUs.routeName,
    "icon": "assets/menu/aboutUs.png"
  }
];

class AppDrawer extends StatelessWidget {
  const AppDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          const Image(image: AssetImage('assets/sidemenu/sidebar.jpg')),
          ...data.map((menu) => _buildDrawerItem(menu, context)),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: const [
              Image(
                image: AssetImage('assets/sidemenu/moh.png'),
                height: 80,
              ),
              Image(
                image: AssetImage('assets/sidemenu/hilmika.png'),
                height: 80,
              )
            ],
          ),
          const Center(
            child: EnatParagraph(body: 'ከጤና ጥበቃ ሚኒስቴር ጋር በመተባበር የተዘጋጀ'),
          ),
        ],
      ),
    );
  }

  Widget _buildDrawerItem(Map<String, String> menu, BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.popAndPushNamed(context, menu['route'].toString());
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(children: [
          Image(
            image: AssetImage(menu['icon'].toString()),
            height: 32,
          ),
          Container(
              margin: const EdgeInsets.only(left: 8),
              child: Text(
                menu["name"].toString(),
                style: Theme.of(context).textTheme.overline,
              ))
        ]),
      ),
    );
  }
}
