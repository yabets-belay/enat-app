import 'package:flutter/material.dart';

class EnatAppBar extends StatelessWidget with PreferredSizeWidget {
  const EnatAppBar({
    Key? key,
  }) : super(key: key);

  @override
  AppBar build(BuildContext context) {
    return AppBar(
      title: const Text('እናት'),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
