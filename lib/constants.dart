import 'package:enat/screens/about_us.dart';
import 'package:enat/screens/due_date_calculator.dart';
import 'package:enat/screens/home_screen.dart';
import 'package:enat/screens/kick_counter.dart';
import 'package:enat/screens/nutrition.dart';
import 'package:enat/screens/registration_screen.dart';
import 'package:enat/screens/symptoms.dart';
import 'package:enat/screens/vaccination.dart';
import 'package:enat/screens/weight_calculator.dart';
import 'package:flutter/material.dart';

const primaryColor = Color.fromRGBO(67, 18, 117, 1);
// the color right down below is for the cards
const cardColor = Color.fromRGBO(208, 29, 97, 1);
const secondaryColor = Color.fromRGBO(253, 237, 242, 1);
const darkPrimaryColor = Color.fromRGBO(190, 31, 87, 1);
const dueDateCardColor = Color.fromRGBO(235, 58, 124, 0.5);
const textColor = Color.fromRGBO(68, 68, 68, 1);

final ThemeData base = ThemeData.light();

ThemeData kThemeData = ThemeData(
    primaryColor: primaryColor,
    colorScheme: base.colorScheme.copyWith(
        primary: primaryColor,
        onPrimary: Colors.white,
        secondary: secondaryColor,
        tertiary: cardColor,
        onSecondary: textColor,
        onTertiary: dueDateCardColor),
    textTheme: base.textTheme.copyWith(
      bodyText1:
          const TextStyle(fontSize: 14, color: textColor, letterSpacing: 1),
      bodyText2: const TextStyle(fontWeight: FontWeight.bold),
      subtitle1: const TextStyle(
        color: Colors.white,
        fontSize: 12,
      ),
      overline: const TextStyle(
          color: textColor, fontWeight: FontWeight.w400, fontSize: 16),
      headline1: const TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 16,
        color: darkPrimaryColor,
      ),
      headline2: const TextStyle(fontSize: 16, color: darkPrimaryColor),
      headline3: const TextStyle(fontSize: 20, color: darkPrimaryColor),
      subtitle2: const TextStyle(fontSize: 32, color: Colors.white),
      caption: const TextStyle(fontSize: 20, color: Colors.white),
    ));

getRoutes(BuildContext context) {
  return {
    RegistrationScreen.routeName: (context) => const RegistrationScreen(),
    HomeScreen.routeName: (context) => const HomeScreen(),
    DueDateCalculator.routeName: (context) => const DueDateCalculator(),
    Nutrition.routeName: (context) => const Nutrition(),
    KickCounter.routeName: (context) => const KickCounter(),
    WeightCalculator.routeName: (context) => const WeightCalculator(),
    Vaccination.routeName: (context) => const Vaccination(),
    Symptoms.routeName: (context) => const Symptoms(),
    AboutUs.routeName: (context) => const AboutUs()
  };
}

const List<String> regions = [
  'Addis Ababa',
  'Diredawa',
  'Oromia',
  'Amhara',
  'Tigray',
  'Afar',
  'Benishangul-Gumuz',
  'Gambella',
  'Harari',
  'Somali',
  'Debub',
  'Sidama',
  'Debub Merab'
];

const List<Map<String, String>> symptomsList = [
  {
    'name': "ከፍተኛ ድካም",
    'key': "symOne",
    'message':
        "ለምን፡ በእርግዝና ወቅት ሰዉነት ለጽንሱ ከፍተኛ የሆነ ሃይል ስለሚያስፈለግና  ሰዉነትሽም ለዚህ ምላሽ ስለሚሰጥ የድካም ስሜት ሊኖር ይችላል፡፡ ይሁንና ድካሙ በጣም ከፍተኛና በእረፍት የማይሻሻል ከሆነ ቶሎ ወደ ጤና ተቋም መሄድና ማማከር ያስፈልጋል፡፡\n"
            "ምን ላድርግ፡ በቂ የሆነ እረፍት ማድረግ፣ አመጋገብን ማስተካከልና ከባድ ስራዎችን አለመስራት",
    'icon': "fatigue"
  },
  {
    'name': "የእንቅልፍ ችግር",
    'key': "symTwo",
    'message': "ለምን፦ ቶሎ ቶሎ ሽንት ቤት በመሄድሽ ፤ በልጅሽ እንቅስቃሴ ወዘተ\n"
        "ምን ላድርግ? \n"
        "- በጊዜ ወደ አልጋ መሄድ ፣\n"
        "- በተመሳሳይ ሰአት መተናት ፣ \n"
        "- ከእንቅልፍ በፊት ብዙ ፈሳሽ አለመጠጣት፣ \n"
        "- ትራስ መጠቀም፦ ሆድሽ ስር፣ በጉልበቶችሽ መሃል \n",
    'icon': "sleepProblem",
  },
  {
    'name': "የትንፉሽ ማጠር",
    'key': "symThree",
    'message': "ለምን ፦ የማህፀንሽ ዕድገት ሌሎች የውስጥ ኦርጋኖችሽን መጫን፣ በልብ ላይ "
        "እርግዝና የሚያመጣው ጫና አንዳንድ ጊዘም የተለያዩ ውስጣዊ  ህመሞች መገለጫ ሊሆን ይችላል።\n"
        "ምን ላድርግ \n"
        "- አቀማመጥ ማስተካከል \n"
        "- ስትተኚ ትራስ መጠቀም \n"
        "- እረፍት መውሰድ \n"
        "- ሃኪምሽን አማክሪ\n",
    'icon': "breathShortening",
  },
  {
    'name': "የቁርጠት ስሜት",
    'key': "symFour",
    'message': "ለምን፦ የማህጸን መወጠር፣ የማህፀን መኮማተር፤ ሀሰተኛ ምጥ፤\n"
        "ምን ላድርግ ፦\n"
        "• አቀማመጥ ማስተካከል\n"
        "• በቂ ፈሳሽ መውሰድ \n"
        "• ተጨማሪ ምልክቶች ካሉ፣ የማይጠፋ ከፍተኛ ቁርጠት ካለ፣ደም ከታየ፣ ሃኪምሽን አማክሪ\n"
        "• የፀባይ ለውጥ\n",
    'icon': "stomach"
  },
  {
    'name': "የጡት ማደግ እና ህመም  ስሜት ",
    'key': "symFour",
    'message': "ከጡት ውሀማ የሆነ ወተት መውጣት ሊኖር ይችላል ።\n"
        "ለምን? በሆርሞን ለውጦች ምክንያት \n"
        "• ምን ላድርግ? \n"
        "• የእርግዝና ጡት ማስያዣ ማድረግ፤ \n"
        "• አስፈላጊ ከሆነ ጡት ማስያዣ ዉስጥ ለዚሁ የተዘጋጀ ፓድ ማድረግ\n",
    'icon': "breastPacing",
  },
  {
    'name': "ሆድ ድርቀት",
    'key': "symFive",
    'message': "ለምን? በ በሆርሞን ለውጦች ምክንያት የአንጀት ጡንቻዎች መፍታታት\n"
        "ምን ላድርግ? \n"
        "• እንቅስቃሴ ማድረግ፤\n"
        "• በቂ ውሀ መጠጣት፤\n"
        "• ፋይበር ያለበት ምግብ መብላት ፡ ለምሳሌ ፍራፍሬ እና የበሰለ አትክልት\n",
    'icon': "stomachache"
  },
  {
    'name': "የደረት ማቃጠል",
    'key': "symSix",
    'message': "ለምን፦ በሚፈጩ አካላት ጡንቻዎች መፍታታት፣  በጨጉዋራ ወደላይ መገፋት\n"
        "ምን ላድርግ? \n"
        "• እንደበላሽ አትተኚ፣ \n"
        "• ሶስቴ በዛ አርጎ ከመብላት  ትንንሽ መጠን ደጋግመሽ ብይ፣ \n"
        "• ቅመም የበዛበት ምግብ አትብዪ \n",
    'icon': "stomachache"
  },
  {
    'name': "የሽንት ቶሎ ቶሎ መምጣት",
    'key': "symSeven",
    'message': "ለምን ፦ ማህጸን  የሽንት ፊኛን መጫን፣ የሆርሞን ለውጥ፣ የሰውነት የውሃ መጠን መጨመር \n"
        "ምን ላድርግ?\n"
        "• በስራ ቦታም ብትሆኚ ሳትሳቀቂ ወደ መፀዳጃ መሄድ\n"
        "• ሳል እና ማስነጠስ ካለ ሽንት በትንሹ ሊያመልጥ ስለሚችል ፓድ ማድረግ\n"
        "• የሽንት ቀለም እና ሽታ መቀየር፣ የሽንት ማቃጠል ካሉ፣ የኢንፌክሽን ምልክቶች ሊሆኑ ስለሚችሉ ሃኪምሽን ማማከር\n"
        "• ቀን በቂ ፈሳሽ መውሰድ ፣ ከእንቅልፍ በፊት መቀነስ\n",
    'icon': "urinate",
  },
  {
    'name': "የወገብ ህመም",
    'key': "symEight",
    'message': "ለምን፦ በመገጣጠሚያዎች መሳሳብ፣ ክብደት ከመጨመር ጋር በተያያዘ፣ የሆርሞን ለውጥ\n"
        "ምን ላርግ?\n"
        "• አቀማመጥ ማስተካከል\n"
        "• ለእርግዝና የተዘጋጁ ትራሶችን መጠቀም ወዘተ\n",
    'icon': "waistPain",
  },
  {
    'name': "የእግር እና የፊት እብጠት ",
    'key': "symNine",
    'message': "ለምን? በሰውነትሽ ውስጥ ያለው የውሃ እና የደም መጠን በእርግዝና ወቅት ስለሚጨምር፣\n"
        "ምን ላድርግ\n"
        "• እግር ከፍ አድርጎ መቀመጥ\n"
        "• የሚመች ጫማ ማድረግ\n"
        "• የሚያጣብቁ አልባሳትን አለማድረግ\n"
        "• በአጭር  ግዜ በጣም ከፍተኛ የሆነ የእብጠት መጨመር ካለ ሐኪምሽን አማክሪ\n",
    'icon': "legSwallowing",
  },
  {
    'name': "የመዳፍ መቆጥቆጥ፣ መደንዘዝ",
    'key': "symTen",
    'message': "ለምን፦ በ  እጅ አንጓ ውስጥ ባላ እብጠት \n"
        "ምን ላድርግ? \n"
        "• በቻልሽው እጅሽን ማሳረፍ፣ ሃኪምሽን ማማከር \n",
    'icon': "slendering"
  },
  {
    'name': "የቆዳ ሸንተረር ",
    'key': "symElven",
    'message': "ለምን? በቆዳ መወጠር \n" "ምን ላድርግ?  መከላከያው አይታወቅም፣ ቆዳሽ ደረቅ አይሁን\n",
    'icon': "stretchMarks",
  },
  {
    'name': "ማቅለሽለሽ እና ማስመለስ ",
    'key': "symTwelve",
    'message': "ለምን፦ ሆርሞን ለውጥ\n"
        "ምን ላድርግ?\n"
        "• ዝንጅብል ለማቅለሽልሽ ይረዳል \n"
        "• ቅባት ያልበዛበት ደረቅ ነገር ማዘውተር\n"
        "• ትንንሽ መጠን ደጋግመሽ ብዪ\n"
        "• ምልክቶች ካልቀነሱ ሃኪምሽን አናግሪ!\n",
    'icon': "nauseaVomiting",
  }
];
