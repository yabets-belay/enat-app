import 'dart:async';
import 'dart:convert';

import 'package:enat/Abushakir/abushakir.dart';
import 'package:enat/widgets/baby_name.dart';
import 'package:enat/widgets/bold_text.dart';
import 'package:enat/widgets/bulletin.dart';
import 'package:enat/widgets/contact.dart';
import 'package:enat/widgets/enat_paragraph.dart';
import 'package:enat/widgets/fullscreen_image.dart';
import 'package:enat/widgets/image_jumbotron.dart';
import 'package:enat/widgets/sub_title_header.dart';
import 'package:enat/widgets/thumbnail.dart';
import 'package:enat/widgets/title_header.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

Map<String, dynamic> parseJson(String inputJson) {
  final parsedJson = jsonDecode(inputJson);
  return {"data": parsedJson};
}

Future<String> readJsonFromFile(String filePath) async {
  String data = await rootBundle.loadString(filePath);
  return data;
}

Future<Map<String, dynamic>> dataFromFile(String path) async {
  String json = await readJsonFromFile(path);
  Map<String, dynamic> data = parseJson(json);
  return data;
}

ListView getContentListView(data) {
  return ListView(
    scrollDirection: Axis.vertical,
    shrinkWrap: true,
    children: [...data['data'].map(_getWidgetByType)],
  );
}

Widget _getWidgetByType(item) {
  switch (item['type']) {
    case 'jumbotron':
      {
        return ImageJumbotron(imgLocation: item['image']);
      }
    case 'title':
      {
        return TitleHeader(title: item['text']);
      }
    case 'subtitle':
      {
        return SubTitleHeader(title: item['text']);
      }
    case 'text':
      {
        return EnatParagraph(body: item['text']);
      }
    case 'bold':
      {
        return BoldText(text: item['text']);
      }
    case 'bulletin':
      {
        return Bulletin(data: item['list']);
      }
    case 'fullscreenImage':
      {
        return FullscreenImage(imageSource: item['image']);
      }
    case 'thumbnail':
      {
        return Thumbnail(
          primaryImage: item['primaryImage'],
          secondaryImage: item['secondaryImage'],
        );
      }
    case 'contact':
      {
        return Contact(name: item['name'], phoneNumber: item['phone']);
      }
    case 'babyName':
      {
        return BabyName(data: item['data']);
      }
    default:
      return emptyWidget();
  }
}

SizedBox emptyWidget() => const SizedBox.shrink();

saveBabyNames(String key, Set<String> ids) async {
  final prefs = await SharedPreferences.getInstance();

  prefs.setString(key, ids.join(","));
}

Future<Set<String>> getBabyNames(String key) async {
  final prefs = await SharedPreferences.getInstance();

  final names = prefs.getString(key) ?? "";

  return Set.of(names.split(","));
}

const dueDateKey = "DUE_DATE";

Future<EtDateTime> getDueDate() async {
  final prefs = await SharedPreferences.getInstance();

  final storedDate = prefs.getString(dueDateKey) ?? getYesterday();

  return EtDateTime.parse(storedDate);
}

void setDueDate(EtDateTime birthday) async {
  final prefs = await SharedPreferences.getInstance();

  prefs.setString(dueDateKey, birthday.toString());
}

String getYesterday() {
  var today = EtDateTime.now();
  var yesterday = today.subtract(const Duration(days: 1));
  return yesterday.toString();
}

int getLastYear() {
  var today = EtDateTime.now();
  return today.year - 1;
}

const symptomsKey = "SYMPTOM";

void addSymptom(String symptomName) async {
  final prefs = await SharedPreferences.getInstance();

  List<String> symptoms = prefs.getStringList(symptomsKey) ?? [];

  var now = EtDateTime.now();
  var dayTime = now.hour / 12 < 1 ? 'ጠዋት' : 'ምሽት';
  var h = (now.hour + 3) % 12;
  if (h == 0) h = 12;

  symptoms.add(
      '$symptomName - ${now.monthGeez} ${now.day} ፤ ${now.year} ${h.toString().padLeft(2, '0')}:${now.minute.toString().padLeft(2, '0')} $dayTime');

  prefs.setStringList(symptomsKey, symptoms);
}

Future<List<String>> getSymptomsHistory() async {
  final prefs = await SharedPreferences.getInstance();

  return prefs.getStringList(symptomsKey) ?? [];
}

void removeSymptomHistory() async {
  await _removeRecordByKey(symptomsKey);
}

Future<void> _removeRecordByKey(String key) async {
  final prefs = await SharedPreferences.getInstance();

  prefs.remove(key);
}

const kickCounterKey = "KICK_COUNTER";
const delimiter = "|";
void addKickCount(List<String> kickCount) async {
  final prefs = await SharedPreferences.getInstance();

  List<String> kicks = prefs.getStringList(kickCounterKey) ?? [];
  kicks.add(kickCount.join(delimiter));

  prefs.setStringList(kickCounterKey, kicks);
}

Future<List<List<String>>> getKickCountHistory() async {
  final prefs = await SharedPreferences.getInstance();

  List<String> kicks = prefs.getStringList(kickCounterKey) ?? [];

  return kicks.map((element) => element.split(delimiter)).toList();
}

void removeKickCountHistory() async {
  _removeRecordByKey(kickCounterKey);
}

/// from https://medium.com/analytics-vidhya/build-a-simple-stopwatch-in-flutter-a1f21cfcd7a8
Stream<int> timerStream() {
  StreamController<int>? streamController;
  Timer? timer;
  Duration timerInterval = const Duration(seconds: 1);
  int counter = 0;

  void tick(_) {
    counter++;
    streamController?.add(counter);
  }

  void stopTimer() {
    timer?.cancel();
    timer = null;
    counter = 0;
    streamController?.close();
  }

  void startTimer() {
    timer = Timer.periodic(timerInterval, tick);
  }

  streamController = StreamController<int>(
    onListen: startTimer,
    onCancel: stopTimer,
    onResume: startTimer,
    onPause: stopTimer,
  );

  return streamController.stream;
}

const registeredKey = "IS_REGISTERED";

Future<bool> isRegistered() async {
  final prefs = await SharedPreferences.getInstance();
  return prefs.getBool(registeredKey) ?? false;
}

void setRegistered() async {
  final prefs = await SharedPreferences.getInstance();
  prefs.setBool(registeredKey, true);
}
