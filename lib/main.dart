import 'package:enat/constants.dart';
import 'package:enat/screens/home_screen.dart';
import 'package:enat/screens/registration_screen.dart';
import 'package:enat/utility.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Firebase.initializeApp();
  runApp(MyApp(
      homeScreen: await isRegistered()
          ? const HomeScreen()
          : const RegistrationScreen()));
}

class MyApp extends StatelessWidget {
  final Widget homeScreen;
  const MyApp({Key? key, required this.homeScreen}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      theme: kThemeData,
      home: homeScreen,
      routes: getRoutes(context),
    );
  }
}
