part of abushakir;

///
/// Interface for Ethiopian Data and Time types.
///
///
abstract class EDT extends Equatable {
  @override
  List<Object> get props => [];

  int get year;

  int get month;

  String get monthGeez;

  int get day;

  String get dayGeez;

  int get hour;

  int get minute;

  int get second;

  int get millisecond;

  /// Methods

  @override
  String toString();

  String toJson();

  String toIso8601String();

  bool isBefore(EtDateTime other);

  bool isAfter(EtDateTime other);

  bool isAtSameMomentAs(EtDateTime other);

  int compareTo(EtDateTime other);

  EtDateTime add(Duration duration);

  EtDateTime subtract(Duration duration);
}

///
/// Interface for Ethiopian Calendar types.
///
///
abstract class Calendar extends Equatable {
  @override
  List<Object> get props => [];

  /// Properties
  int get year;

  int get month;

  int get day;

  String get monthName;

  monthDays();

  yearDays();
}
