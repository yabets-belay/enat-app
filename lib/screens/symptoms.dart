import 'package:enat/widgets/enat_button.dart';
import 'package:enat/widgets/enat_paragraph.dart';
import 'package:enat/widgets/enat_table.dart';
import 'package:enat/widgets/image_jumbotron.dart';
import 'package:flutter/material.dart';

import '../constants.dart';
import '../utility.dart';
import '../widgets/cancel_button.dart';

class Symptoms extends StatefulWidget {
  static const String routeName = "/symptoms";

  const Symptoms({Key? key}) : super(key: key);

  @override
  State<Symptoms> createState() => _SymptomsState();
}

class _SymptomsState extends State<Symptoms> {
  List<String> _history = [];

  final String imgLocation = 'assets/sidemenu/symptoms.jpg';

  @override
  void initState() {
    super.initState();
    _getSymptomHistory();
  }

  _getSymptomHistory() async {
    var val = await getSymptomsHistory();
    setState(() {
      _history = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            bottom: _getTabBar(),
          ),
          body: _getTabBarView(context),
        ));
  }

  _getTabBar() {
    return const TabBar(
      tabs: [Tab(text: 'ምልክቶች'), Tab(text: 'የተመዘገቡ')],
    );
  }

  _getTabBarView(BuildContext context) {
    return TabBarView(
      children: [
        _buildSymptomsListView(context),
        _buildSymptomsHistory(),
      ],
    );
  }

  _buildSymptomsListView(BuildContext context) {
    return ListView(
      children: [
        ..._buildCommonHeader(),
        ..._buildButtonsList(context),
      ],
    );
  }

  _buildSymptomsHistory() {
    var successMessage = 'ሁሉም የተመዘገቡ ምልክች ተወግደዋል፡፡';
    var contentBody = 'ሁሉም የተመዘገቡ ምልክች ይጠፋሉ፡፡';
    return ListView(
      children: [
        ..._buildCommonHeader(),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: EnatTable(data: [
            ..._history.map((e) => [e])
          ]),
        ),
        const SizedBox(
          height: 16.0,
        ),
        _buildResetTableButton(successMessage, contentBody, _clearSymptoms),
        const SizedBox(
          height: 16.0,
        )
      ],
    );
  }

  Widget _buildResetTableButton(
      String successMessage, String contentBody, cancelAction) {
    return CancelButton(
        contentBody: contentBody,
        successMessage: successMessage,
        cancelAction: cancelAction);
  }

  void _clearSymptoms() {
    removeSymptomHistory();
    _getSymptomHistory();
  }

  _buildImageJumbotron() {
    return ImageJumbotron(imgLocation: imgLocation);
  }

  _buildDescriptionText() {
    return const EnatParagraph(
        body:
            'በእርግዝና ጊዜ ሊታዩ የሚችሉ ምልክቶችን ይነግርሻል። ከታች ከተዘረዘሩት ስሜቶች በሚሰማሽ ጊዜ ተጭነሽ ምክንያቱ ምን እንደሆነና ለሃኪምሽም ለማሳየት መመዝገብ ያስችልሻል');
  }

  _buildCommonHeader() {
    return <Widget>[
      _buildImageJumbotron(),
      _buildDescriptionText(),
    ];
  }

  _buildButtonsList(BuildContext context) {
    return symptomsList.map((symptom) => Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: EnatButton(
              text: symptom['name'].toString(),
              onPressed: () {
                return showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: const Text('መልእክት'),
                        content:
                            EnatParagraph(body: symptom['message'].toString()),
                        actions: [
                          TextButton(
                              child: const Text('መዝግብ'),
                              onPressed: () {
                                Navigator.of(context).pop();
                                addSymptom(symptom['name'].toString());
                                _getSymptomHistory();
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(SnackBar(
                                  content: Text('${symptom['name']} ተመዝግቧል፡፡'),
                                ));
                              }),
                          TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: const Text('ይቅር'))
                        ],
                      );
                    });
              }),
        ));
  }
}
