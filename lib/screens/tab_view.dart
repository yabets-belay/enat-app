import 'package:enat/widgets/card_item.dart';
import 'package:flutter/material.dart';

class TabView extends StatelessWidget {
  /// data will have the following structure
  /*
  [
    // the following is one card in the tab
    {
      "header" // card title
      "image" // location to the card image
      "data": [ // the detail screen content is stored as list of widgets
        {"id", "type", "value"} // detail screen widgets
       ]
     }, ...
   ]
   */
  final List data;
  const TabView({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.count(crossAxisCount: 2, childAspectRatio: 1.05, children: [
      ...data.map(
        (item) => CardItem(data: item),
      )
    ]);
  }
}
