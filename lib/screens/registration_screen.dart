import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enat/screens/home_screen.dart';
import 'package:enat/utility.dart';
import 'package:flutter/material.dart';

import '../constants.dart';
import '../widgets/enat_app_bar.dart';

class RegistrationScreen extends StatefulWidget {
  static const String routeName = "/registration";

  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  State<RegistrationScreen> createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  String _name = '';
  int _age = 0;
  String _phone = '';
  String _email = '';
  String _location = regions[0];

  @override
  Widget build(BuildContext context) {
    void _popAndPush() {
      Navigator.popAndPushNamed(context, HomeScreen.routeName);
    }

    return Scaffold(
      appBar: const EnatAppBar(),
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: ListView(
              children: [
                const Text('Registration form'),
                TextFormField(
                  style: Theme.of(context).textTheme.bodyText1,
                  decoration: const InputDecoration(
                    labelText: 'Name',
                    errorStyle: TextStyle(fontSize: 12.0),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Name is required';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _name = value!;
                  },
                ),
                TextFormField(
                  style: Theme.of(context).textTheme.bodyText1,
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(
                    labelText: 'Age',
                    errorStyle: TextStyle(fontSize: 12.0),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Age is required';
                    }
                    try {
                      var val = int.parse(value);
                      if (val <= 1 || val >= 100) {
                        return 'Age መጠን የሚቀበለው ከ1 እስከ 100 ድረስ ያሉ ቁጥሮችን ብቻ ነው፡፡';
                      }
                    } catch (e) {
                      return 'Age መጠን የሚቀበለው ከ1 እስከ 100 ድረስ ያሉ ቁጥሮችን ብቻ ነው፡፡';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _age = int.parse(value!);
                  },
                ),
                TextFormField(
                  style: Theme.of(context).textTheme.bodyText1,
                  keyboardType: TextInputType.phone,
                  decoration: const InputDecoration(
                    labelText: 'Phone',
                    hintText: '09xx xxxxxx',
                    errorStyle: TextStyle(fontSize: 12.0),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Phone number is required';
                    }
                    if (value.length < 9) {
                      return 'Please input a valid phone number';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _phone = value!;
                  },
                ),
                TextFormField(
                  style: Theme.of(context).textTheme.bodyText1,
                  keyboardType: TextInputType.emailAddress,
                  decoration: const InputDecoration(
                    labelText: 'Email',
                  ),
                  onSaved: (value) {
                    _email = value!;
                  },
                ),
                Row(children: [
                  Text(
                    'Region',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  const SizedBox(
                    width: 8.0,
                  ),
                  DropdownButton<String>(
                    style: Theme.of(context).textTheme.overline,
                    value: _location,
                    icon: const Icon(Icons.arrow_drop_down),
                    items: regions
                        .map((r) => DropdownMenuItem(value: r, child: Text(r)))
                        .toList(),
                    onChanged: (value) {
                      setState(() {
                        _location = value!;
                      });
                    },
                  ),
                ]),
                TextButton(
                    onPressed: () async {
                      if (_formKey.currentState?.validate() == true) {
                        _formKey.currentState?.save();
                        await registerUser(
                            _name, _age, _phone, _email, _location);
                        _popAndPush();
                      }
                    },
                    child: const Text('Register'))
              ],
            ),
          ),
        ),
      ),
    );
  }

  registerUser(name, age, phone, email, location) {
    setRegistered();
    try {
      FirebaseFirestore.instance
          .collection('users')
          .add({
            'name': name,
            'age': age,
            'phone': phone,
            'email': email,
            'location': location
          })
          .then((value) => setRegistered())
          .catchError((error) {
            setRegistered();
          });
    } catch (e) {
      setRegistered();
      return;
    }
  }
}
