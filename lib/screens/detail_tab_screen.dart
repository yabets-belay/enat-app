import 'package:flutter/material.dart';

import '../utility.dart';

class DetailTabScreen extends StatelessWidget {
  /// data has the following structure
  /*
  {
    "header": "ሳምንታዊ የልጅሽ አቀማመጥና እድገቶች",
    "back": "lanchi",
    "image": three,
    "tabs": [
          {
            "name": "1ኛ ሳምንት",
            "data": [
            {
              "id": 1,
              "type":"thumbnail"
            },
            ... rest of tab content
          ]
         },
         ... rest of tabs
     ]
   }
   */
  final Map<String, dynamic> data;
  const DetailTabScreen({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List tabs = data['tabs'];
    int size = tabs.length;
    return DefaultTabController(
        length: size,
        child: Scaffold(
          appBar: AppBar(
            title: Text(data['header']),
            bottom: _getTabBar(),
          ),
          body: _getTabBarView(),
        ));
  }

  _getTabBar() {
    return TabBar(
      isScrollable: true,
      tabs: [
        ...data['tabs'].map((tab) => Tab(
              text: tab['name'],
            ))
      ],
    );
  }

  _getTabBarView() {
    return TabBarView(
        children: [...data['tabs'].map((tab) => getContentListView(tab))]);
  }
}
