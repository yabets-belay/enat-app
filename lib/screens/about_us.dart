import 'package:enat/widgets/enat_app_bar.dart';
import 'package:enat/widgets/enat_paragraph.dart';
import 'package:flutter/material.dart';

import '../widgets/image_jumbotron.dart';

class AboutUs extends StatelessWidget {
  static const String routeName = "/about-us";

  final String imgLocation = 'assets/sidemenu/selegna.jpg';
  const AboutUs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const EnatAppBar(),
      body: _buildBody(),
    );
  }

  ListView _buildBody() {
    return ListView(children: [
      ImageJumbotron(imgLocation: imgLocation),
      const EnatParagraph(
        body: 'እናት የሞባይል መተግበርያ በህልሚካ ቴክ ሶሉሽን ሃላ/የተ/የግ/ማህበርና በኢትዮጵያ ጤና '
            'ጥበቃ ሚኒስትር ትብብር ተዘጋጅቶ ላንቺ የቀረበ ነው። በዚህ ሞባይል መተግበርያ  '
            'ውስጥ ያሉ መረጃዎች በሙሉ በጥንቃቄ በጤና ጥበቃ ሚኒስቴር ባለሙያዎችና በዚህ '
            'ሙያ ላይ ከፍተኛ እውቀትና ልምድ ባላቸው ባለሙያዎች ተዘጋጅቶና ተፈትሾ የቀረበ '
            'ነው። እናት የሞባይል መተግበርያ ከቤተሰብ እቅድ ጀምሮ ሙሉ የእርግዝና ጊዜሽንና '
            'ከወለድሽም በኋላ እራስሽንና ልጅሽን እንዴት መንከባከብ እንዳለብሽ የሚያስተምርና '
            'የሚከታተልሽ የቴክኖሎጂ ውጤት ነው። ይህ የሞባይል መተግበርያ ፈጽሞ ዶክተርን '
            'አይተካም። ይህንን መረጃ በህትመትም ሆነ በማናቸውም የመረጃ ዘዴዎች ያለ ፈቃድ '
            'መውሰድ ወይም ማባዛት ክልክል ነው።',
      ),
    ]);
  }
}
