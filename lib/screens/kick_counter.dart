import 'dart:async';

import 'package:enat/Abushakir/abushakir.dart';
import 'package:enat/utility.dart';
import 'package:enat/widgets/enat_paragraph.dart';
import 'package:enat/widgets/enat_table.dart';
import 'package:flutter/material.dart';

import '../widgets/cancel_button.dart';
import '../widgets/enat_app_bar.dart';
import '../widgets/kick_progress_indicator.dart';

class KickCounter extends StatefulWidget {
  static const String routeName = "/kick-counter";

  const KickCounter({Key? key}) : super(key: key);

  @override
  State<KickCounter> createState() => _KickCounterState();
}

class _KickCounterState extends State<KickCounter> {
  int _count = 0;
  List<List<String>> _data = [];
  late Stream<int> _timerStream;
  late StreamSubscription<int> _timerSubscription;
  String _hrStr = '00';
  String _minStr = '00';
  String _secStr = '00';
  late EtDateTime _startTime;

  @override
  void initState() {
    super.initState();
    _getKickCountHistory();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const EnatAppBar(),
      body: _buildBody(),
    );
  }

  _buildBody() {
    return ListView(
      children: [
        _buildDescription(),
        _buildProgressBar(),
        _buildKickCounter(),
        _buildTimer(),
        _buildTable(),
        _buildResetButton(),
      ],
    );
  }

  _buildDescription() {
    return const EnatParagraph(body: 'የልጅዎን እንቅስቃሴ ይቆጣጠሩ');
  }

  _buildProgressBar() {
    return KickProgressIndicator(currentCount: _count);
  }

  _buildKickCounter() {
    return Center(
      child: Stack(
        children: [_buildFingerprintButton(), _buildCountBadge()],
      ),
    );
  }

  TextButton _buildFingerprintButton() {
    return TextButton(
        style: TextButton.styleFrom(
          padding: const EdgeInsets.all(16.0),
        ),
        onPressed: _handleFingerprintPress,
        child: const Icon(
          Icons.fingerprint,
          size: 140.0,
        ));
  }

  _buildCountBadge() {
    return Positioned(
      bottom: 0,
      right: 0,
      child: Container(
        padding: const EdgeInsets.all(16.0),
        // margin: const EdgeInsets.only(left: -16.0),
        // alignment: Alignment.bottomRight,

        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.secondary,
          borderRadius: const BorderRadius.all(Radius.circular(32.0)),
        ),
        child: Text(
          '$_count',
          style: TextStyle(fontSize: 28, color: Theme.of(context).primaryColor),
        ),
      ),
    );
  }

  _buildTimer() {
    return Center(child: Text('$_hrStr:$_minStr:$_secStr'));
  }

  _buildTable() {
    return Padding(
        padding: const EdgeInsets.all(16.0), child: EnatTable(data: _data));
  }

  _buildResetButton() {
    if (_data.isEmpty) {
      return emptyWidget();
    }
    var successMessage = 'ሁሉም የተመዘገቡ የልጅዎ እንቅስቃሴ ተወግደዋል፡፡';
    var contentBody = 'ሁሉም የተመዘገቡ የልጅዎ እንቅስቃሴ ይጠፋሉ፡፡';
    return CancelButton(
        contentBody: contentBody,
        successMessage: successMessage,
        cancelAction: _handleResetData);
  }

  _handleResetData() {
    removeKickCountHistory();
    _getKickCountHistory();
    setState(() {
      _data = [];
    });
  }

  void _handleFingerprintPress() {
    if (_count == 0) {
      _resetTimer();
    }
    var newCount = _count + 1;
    setState(() {
      _count = newCount;
    });
    if (newCount == 10) {
      List<String> newKickCount = [
        '${_startTime.monthGeez} ${_startTime.day}፤${_startTime.year}',
        '${_startTime.hour + 3}:${_startTime.minute}',
        '$_hrStr:$_minStr:$_secStr',
        '$_count'
      ];
      addKickCount(newKickCount);
      _getKickCountHistory();
      _stopTimer();
    }
  }

  void _resetTimer() {
    _timerStream = timerStream();
    _timerSubscription = _timerStream.listen((int newTick) {
      setState(() {
        _hrStr = ((newTick / 3600) % 60).floor().toString().padLeft(2, '0');
        _minStr = ((newTick / 60) % 60).floor().toString().padLeft(2, '0');
        _secStr = (newTick % 60).floor().toString().padLeft(2, '0');
        _startTime = EtDateTime.now();
      });
    });
  }

  void _stopTimer() {
    _timerSubscription.cancel();
    setState(() {
      _hrStr = '00';
      _minStr = '00';
      _secStr = '00';
      _count = 0;
    });
  }

  void _getKickCountHistory() async {
    var val = await getKickCountHistory();
    setState(() {
      _data = val;
    });
  }
}
