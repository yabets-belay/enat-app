import 'package:flutter/material.dart';

import '../utility.dart';

class DetailScreen extends StatelessWidget {
  final Map<String, dynamic> data;
  const DetailScreen({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
        data['header'].toString(),
        style: const TextStyle(fontSize: 14),
      )),
      body: getContentListView(data),
    );
  }
}
