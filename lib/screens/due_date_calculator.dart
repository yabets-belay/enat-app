import 'package:enat/Abushakir/abushakir.dart';
import 'package:enat/screens/home_screen.dart';
import 'package:enat/utility.dart';
import 'package:enat/widgets/date_picker.dart';
import 'package:enat/widgets/enat_button.dart';
import 'package:enat/widgets/enat_paragraph.dart';
import 'package:enat/widgets/header3.dart';
import 'package:enat/widgets/image_jumbotron.dart';
import 'package:flutter/material.dart';

import '../widgets/enat_app_bar.dart';

class DueDateCalculator extends StatefulWidget {
  static const String routeName = "/due-date";

  const DueDateCalculator({Key? key}) : super(key: key);

  @override
  State<DueDateCalculator> createState() => _DueDateCalculatorState();
}

class _DueDateCalculatorState extends State<DueDateCalculator> {
  EtDateTime birthday = EtDateTime.now();

  void setBirthday(newBirthday) {
    setState(() {
      birthday = newBirthday;
    });
  }

  @override
  Widget build(BuildContext context) {
    const imgLocation = "assets/sidemenu/duedate.jpg";
    const body = 'ይህ የመውለጃ ቀንሽን እና ምን ያህል ሳምንት እንደቀረሽ ለማወቅ የሚረዳሽ የቀን ማስያ ነው።';
    const orText = 'ወይም';
    const pickBirthday = 'የመውለጃ ቀንሽን ምረጪ';
    const pickLastPeriodDay = 'የመጨረሻ የወር አበባ ቀን ምረጪ';
    return Scaffold(
      appBar: const EnatAppBar(),
      body: ListView(
        children: [
          const ImageJumbotron(imgLocation: imgLocation),
          const EnatParagraph(body: body),
          const Header3(title: pickBirthday),
          _buildDatePicker(),
          Center(
              child: EnatButton(
                  key: const Key('birthdayBtn'),
                  text: 'አስገቢ',
                  onPressed: () {
                    setDueDate(birthday);
                    Navigator.popAndPushNamed(context, "/");
                  })),
          const Center(
              child: EnatParagraph(
            body: orText,
          )),
          const Header3(title: pickLastPeriodDay),
          _buildDatePicker(),
          Center(
              child: EnatButton(
                  key: const Key('periodBtn'),
                  text: 'አስገቢ',
                  onPressed: () {
                    // from last period day there are 40 weeks
                    setDueDate(birthday.add(const Duration(days: 280)));
                    Navigator.popAndPushNamed(context, HomeScreen.routeName);
                  })),
        ],
      ),
    );
  }

  DatePicker _buildDatePicker() =>
      DatePicker(etDate: birthday, setDate: setBirthday);
}
