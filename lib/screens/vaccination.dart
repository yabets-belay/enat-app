import 'package:enat/Abushakir/abushakir.dart';
import 'package:enat/utility.dart';
import 'package:enat/widgets/date_picker.dart';
import 'package:enat/widgets/enat_button.dart';
import 'package:enat/widgets/enat_paragraph.dart';
import 'package:enat/widgets/enat_switch.dart';
import 'package:enat/widgets/enat_table.dart';
import 'package:enat/widgets/header3.dart';
import 'package:enat/widgets/image_jumbotron.dart';
import 'package:flutter/material.dart';

import '../widgets/enat_app_bar.dart';

class Vaccination extends StatefulWidget {
  static const String routeName = "/vaccination";

  const Vaccination({Key? key}) : super(key: key);

  @override
  State<Vaccination> createState() => _VaccinationState();
}

class _VaccinationState extends State<Vaccination> {
  var _selectedDate = EtDateTime.now();
  var _motherVaccine = false;
  List<List<String>> _data = [];
  var _title = 'የልጅሽ ውልደት ቀን';

  void setBirthday(newBirthday) {
    setState(() {
      _selectedDate = newBirthday;
    });
  }

  get imgLocation => 'assets/sidemenu/vaccination.jpg';

  get description => 'ይህ የቀን ማስያ ክትባት መቼ መከተብ እንዳለብሽ ይነግርሻል';

  get title => _title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const EnatAppBar(),
      body: _buildBody(),
    );
  }

  _buildBody() {
    return ListView(
      children: [
        ImageJumbotron(imgLocation: imgLocation),
        EnatParagraph(body: description),
        _buildPersonSwitch(),
        Header3(title: title),
        DatePicker(etDate: _selectedDate, setDate: setBirthday),
        _buildCalculateButton(),
        EnatTable(data: _data),
        _buildRemark(),
        const SizedBox(
          height: 16.0,
        ),
      ],
    );
  }

  Widget _buildCalculateButton() {
    return Center(
      child: EnatButton(
          text: 'የክትባት ቀናትን አስብ', onPressed: _calculateVaccinationDate),
    );
  }

  _calculateVaccinationDate() {
    List<List<String>> vaccines = [];
    if (_motherVaccine) {
      vaccines = _getMotherVaccinationDate();
    } else {
      vaccines = _getChildVaccinationDate();
    }
    setState(() {
      _data = vaccines;
    });
  }

  _getMotherVaccinationDate() {
    final motherVaccinationDates = [0, 4, 30, 82, 134];
    final motherVaccineMessage = [
      'በመጀመሪያዎቹ ክትትሎችሽ እንደ ሐኪምሽ ትዕዛዝ',
      'ከቲቲ 1 አራት  ሳምንት በሁዋላ ',
      'ከቲቲ 2 ስድስት ወር በሁዋላ',
      'ከቲቲ 3 ቢያንስ 1 አመት በሁዋላ ወይም ቀጣይ እርግዝና',
      'ከቲቲ 4 ቢያንስ አንድ አመት በሁዋላ ካልሆነም በቀጣይ እርግዝና',
    ];
    List<List<String>> vaccines = [
      [
        'የክትባት ቀን',
        'ክትባት',
      ]
    ];
    for (int i = 0; i < motherVaccinationDates.length; i++) {
      final vaccinationDate =
          _selectedDate.add(Duration(days: motherVaccinationDates[i] * 7));
      List<String> vaccine = [];
      vaccine.add('ቲቲ ${i + 1}');
      vaccine.add(
          '${vaccinationDate.monthGeez} ${vaccinationDate.day} ፤ ${vaccinationDate.year} - ${motherVaccineMessage[i]}');
      vaccines.add(vaccine);
    }
    return vaccines;
  }

  _getChildVaccinationDate() {
    final childVaccinationDates = [0, 6, 10, 14, 39];
    final childVaccines = [
      'BCG, OPV0',
      'DTP-HepB1-Hib1 ,OPV1,PCV1, Rota1',
      '3DTP-HepB2-Hib2,OPV2,PCV2, Rota2 ',
      'DTP-HepB3-Hb3, OPV3, PCV3, IPV',
      'Measles',
    ];
    final childVaccineMessage = [
      'ስትወለጂ',
      'ስድስት ሳምንት',
      'አስር ሳምንት',
      'አስራ አራት ሳምንት',
      'ዘጠኝ ወር',
    ];
    List<List<String>> vaccines = [
      [
        'የክትባት ቀን',
        'ክትባት',
      ]
    ];
    for (int i = 0; i < childVaccinationDates.length; i++) {
      final vaccinationDate =
          _selectedDate.add(Duration(days: childVaccinationDates[i] * 7));
      List<String> vaccine = [];
      vaccine.add(childVaccines[i]);
      vaccine.add(
          '${vaccinationDate.monthGeez} ${vaccinationDate.day} ፤ ${vaccinationDate.year} - ${childVaccineMessage[i]}');
      vaccines.add(vaccine);
    }
    vaccines.add(['ቫይታሚን ኤ', 'ከ6 እስከ 59 ወር']);
    return vaccines;
  }

  EnatSwitch _buildPersonSwitch() {
    return EnatSwitch(
        leftLabel: 'ለልጅሽ',
        rightLabel: 'ላንቺ',
        value: _motherVaccine,
        onChange: (value) {
          setState(() {
            _motherVaccine = value;
            _data = [];
            if (value) {
              _title = 'የመጀመሪያ ክትባትሽን የወሰድሽበት ቀን';
            } else {
              _title = 'የልጅሽ ውልደት ቀን';
            }
          });
        });
  }

  Widget _buildRemark() {
    if (_motherVaccine && _data.isNotEmpty) {
      return Column(
        children: const [
          EnatParagraph(body: 'ቫይታሚን ኤ ለሁሉም ለወለዱ እናቶች'),
          EnatParagraph(body: 'ቲቲ=  ቴታነስ ቶክሶይድ'),
          EnatParagraph(body: 'ቲቲ አምስት ግዜ ከተከተብሽ ልጅ ለመዉለጃ እድሜሽ በሙሉ ያገለግላል።'),
        ],
      );
    }
    return emptyWidget();
  }
}
