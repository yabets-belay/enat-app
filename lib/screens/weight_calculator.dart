import 'package:enat/widgets/enat_button.dart';
import 'package:enat/widgets/enat_paragraph.dart';
import 'package:enat/widgets/header3.dart';
import 'package:flutter/material.dart';

import '../widgets/enat_app_bar.dart';
import '../widgets/image_jumbotron.dart';

class WeightCalculator extends StatefulWidget {
  static const String routeName = "/weight-calculator";

  const WeightCalculator({Key? key}) : super(key: key);

  @override
  State<WeightCalculator> createState() => _WeightCalculatorState();
}

class _WeightCalculatorState extends State<WeightCalculator> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  double _weight = 0.0;
  double _height = 0.0;
  String _message = '';

  get imgLocation => 'assets/sidemenu/weight.jpg';

  get title => 'የክብደት ማሰቢያ';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const EnatAppBar(),
      body: _buildBody(),
    );
  }

  _buildBody() {
    return ListView(
      children: [
        ImageJumbotron(imgLocation: imgLocation),
        Header3(title: title),
        _buildForm(),
      ],
    );
  }

  _buildForm() {
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          children: [
            TextFormField(
              style: Theme.of(context).textTheme.bodyText1,
              keyboardType: const TextInputType.numberWithOptions(),
              decoration: const InputDecoration(
                hintText: 'ለምሳሌ 53',
                labelText: 'ክብደትሽ በኪ.ግ.',
                errorStyle: TextStyle(fontSize: 12),
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'ትክክለኛ የሆነውን የክብደት መጠን ማስገባት አለብሽ፡፡';
                }

                try {
                  var val = double.parse(value);
                  if (val <= 0 || val > 200) {
                    return 'የክብደት መጠን የሚቀበለው ከ0 እስከ 200 ድረስ ያሉ ቁጥሮችን ብቻ ነው፡፡ ';
                  }
                } catch (e) {
                  return 'ያስገባሽው ክብደት መጠን የተሳሳተ ነው፡፡ የክብደት መጠን የሚቀበለው ከ0 እስከ 200 ድረስ ያሉ ቁጥሮችን ብቻ ነው፡፡ ';
                }

                return null;
              },
              onSaved: (value) {
                setState(() {
                  _weight = double.parse(value!);
                });
              },
            ),
            TextFormField(
              style: Theme.of(context).textTheme.bodyText1,
              keyboardType: const TextInputType.numberWithOptions(),
              decoration: const InputDecoration(
                hintText: 'ለምሳሌ 1.53',
                labelText: 'ቁመትሽ በሜ.',
                errorStyle: TextStyle(fontSize: 12),
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'ትክክለኛ የሆነውን የቁመት መጠን ማስገባት አለብሽ፡፡';
                }
                try {
                  var val = double.parse(value);
                  if (val <= 0 || val >= 3) {
                    return 'የቁመት መጠን የሚቀበለው ከ0 እስከ 3.0ሜ ድረስ ያሉ ቁጥሮችን ብቻ ነው፡፡';
                  }
                } catch (e) {
                  return 'የቁመት መጠን የሚቀበለው ከ0 እስከ 3.0ሜ ድረስ ያሉ ቁጥሮችን ብቻ ነው፡፡';
                }

                return null;
              },
              onSaved: (value) {
                setState(() {
                  _height = double.parse(value!);
                });
              },
            ),
            EnatButton(
                text: 'አስብ',
                onPressed: () {
                  setState(() {
                    _message = '';
                  });
                  if (_formKey.currentState!.validate()) {
                    _formKey.currentState?.save();
                    _calculateBMI();
                  }
                }),
            _buildResultDisplay(),
          ],
        ),
      ),
    );
  }

  EnatParagraph _buildResultDisplay() {
    return EnatParagraph(
      body: _message,
    );
  }

  _calculateBMI() {
    var bmi = (_weight / (_height * _height));

    if (bmi < 18.5) {
      setState(() {
        _message = 'የክብደት መጥን፡ ዝቅተኛ ክብደት፤ '
            'ጠቅላላ የሚመከር የክብደት ጭማሪ በዘጠኝ ወራት ውስጥ ከ12.7ኪግ እስከ 18.14ኪግ፡፡ '
            'ይህ ማለትም በሁለተኛው 3 ወራት እና በሶስተኛው ሶስት ወራት(ከ3ወር እስከ 6ወር) በሳምንት በአማካይ 0.59ኪግ መጨመር ይመከራል፡፡';
      });
    } else if (bmi <= 24.9) {
      setState(() {
        _message = 'የክብደት መጥን፡ መደበኛ ክብደት፤ '
            'ጠቅላላ የሚመከር የክብደት ጭማሪ በዘጠኝ ወራት ውስጥ ከ11.34ኪግ እስከ 15.87ኪግ፡፡ '
            'ይህ ማለትም በሁለተኛው 3 ወራት እና በሶስተኛው ሶስት ወራት(ከ3ወር እስከ 6ወር) በሳምንት በአማካይ 0.59ኪግ መጨመር ይመከራል፡፡';
      });
    } else if (bmi <= 29.9) {
      setState(() {
        _message = 'የክብደት መጥን፡ ከፍተኛ ክብደት፤ '
            'ጠቅላላ የሚመከር የክብደት ጭማሪ በዘጠኝ ወራት ውስጥ ከ6.8ኪግ እስከ 11.34ኪግ፡፡ '
            'ለመንታ እርግዝና ከ16.78ኪግ እስከ 24.49ኪግ የክብደት መጠን መጨመር ይመከራል፡፡ '
            'ይህ ማለትም በሁለተኛው 3 ወራት እና በሶስተኛው ሶስት ወራት(ከ3ወር እስከ 6ወር) በሳምንት በአማካይ ከ0.36ኪግ እስከ 0.45ኪግ መጨመር ይመከራል፡፡';
      });
    } else {
      setState(() {
        _message = 'የክብደት መጥን፡ በጣም ከፍተኛ ክብደት፤ '
            'ጠቅላላ የሚመከር የክብደት ጭማሪ በዘጠኝ ወራት ውስጥ ከ4.99ኪግ እስከ 9.07ኪግ፡፡ '
            'ለመንታ እርግዝና ከ11.34ኪግ እስከ 19.05ኪግ የክብደት መጠን መጨመር ይመከራል፡፡ '
            'ይህ ማለትም በሁለተኛው 3 ወራት እና በሶስተኛው ሶስት ወራት(ከ3ወር እስከ 6ወር) በሳምንት በአማካይ ከ0.18ኪግ እስከ 0.27ኪግ መጨመር ይመከራል፡፡';
      });
    }
  }
}
