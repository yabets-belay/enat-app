import 'package:enat/screens/tab_view.dart';
import 'package:enat/widgets/app_drawer.dart';
import 'package:flutter/material.dart';

import '../utility.dart';
import '../widgets/enat_app_bar.dart';
import '../widgets/home_due_date.dart';

class HomeScreen extends StatefulWidget {
  static const String routeName = "/home";

  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: tabs.length,
      child: Builder(builder: (BuildContext context) {
        return Scaffold(
          appBar: const EnatAppBar(),
          body: TabBarView(children: _getTabBarView(context)),
          bottomNavigationBar: BottomAppBar(
              color: Theme.of(context).primaryColor,
              child: const TabBar(
                tabs: tabs,
                isScrollable: true,
              )),
          drawer: const AppDrawer(),
        );
      }),
    );
  }
}

_getTabBarView(BuildContext context) {
  return <Widget>[
    const HomeDueDate(),
    FutureBuilder(
      future: dataFromFile('assets/tabs/lanchi/data.json'),
      initialData: const {'data': []},
      builder: (BuildContext c, AsyncSnapshot<Map<String, dynamic>> json) =>
          TabView(data: json.data!['data']),
    ),
    FutureBuilder(
      future: dataFromFile('assets/tabs/lelegesh/data.json'),
      initialData: const {'data': []},
      builder: (BuildContext c, AsyncSnapshot<Map<String, dynamic>> json) =>
          TabView(data: json.data!['data']),
    ),
    FutureBuilder(
      future: dataFromFile('assets/tabs/leagaresh/data.json'),
      initialData: const {'data': []},
      builder: (BuildContext c, AsyncSnapshot<Map<String, dynamic>> json) =>
          TabView(data: json.data!['data']),
    ),
    FutureBuilder(
      future: dataFromFile('assets/tabs/familyplanning/data.json'),
      initialData: const {'data': []},
      builder: (BuildContext c, AsyncSnapshot<Map<String, dynamic>> json) =>
          TabView(data: json.data!['data']),
    ),
    FutureBuilder(
      future: dataFromFile('assets/tabs/betawkiw/data.json'),
      initialData: const {'data': []},
      builder: (BuildContext c, AsyncSnapshot<Map<String, dynamic>> json) =>
          TabView(data: json.data!['data']),
    ),
  ];
}

const List<Tab> tabs = <Tab>[
  Tab(
    icon: Image(
      image: AssetImage('assets/menu/dueDate.png'),
      height: 32,
    ),
    text: 'የመውለጃ ቀን',
  ),
  Tab(
      icon: Image(
        image: AssetImage('assets/menu/mother.png'),
        height: 32,
      ),
      text: 'ላንቺ'),
  Tab(
      icon: Image(
        image: AssetImage('assets/menu/baby.png'),
        height: 32,
      ),
      text: 'ለልጅሽ'),
  Tab(
      icon: Image(
        image: AssetImage('assets/menu/partner.png'),
        height: 32,
      ),
      text: 'ላጋርሽ'),
  Tab(
    icon: Image(
      image: AssetImage('assets/menu/familyPlanning.png'),
      height: 32,
    ),
    text: 'የቤተሰብ ዕቅድ',
  ),
  Tab(
      icon: Image(
        image: AssetImage('assets/menu/idea.png'),
        height: 32,
      ),
      text: 'ብታውቂው'),
];
