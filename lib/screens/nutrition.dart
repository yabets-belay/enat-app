import 'package:enat/widgets/enat_button.dart';
import 'package:enat/widgets/enat_paragraph.dart';
import 'package:enat/widgets/enat_switch.dart';
import 'package:enat/widgets/header3.dart';
import 'package:enat/widgets/image_jumbotron.dart';
import 'package:flutter/material.dart';

import '../widgets/enat_app_bar.dart';

class Nutrition extends StatefulWidget {
  static const String routeName = "/nutrition";

  const Nutrition({Key? key}) : super(key: key);

  @override
  State<Nutrition> createState() => _NutritionState();
}

enum Milk { breast, powder }

class _NutritionState extends State<Nutrition> {
  final List<String> ageGroup = [
    'ከ1-4 ቀን',
    'ከ5ቀን - 3ወር',
    'ከ3-6 ወር',
    'ከ6 - 12 ወር'
  ];
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final String title = 'የልጅሽ አመጋገብ አስቢ';
  final String imgLocation = 'assets/sidemenu/feed.jpg';

  late bool _isPowderMilk;
  late int _selectedAgeGroup;
  late double _childWeight;
  late String _message;
  late TextEditingController _controller;
  String? _errorMessage;

  @override
  void initState() {
    super.initState();
    _isPowderMilk = false;
    _selectedAgeGroup = 0;
    _childWeight = 0;
    _message = '';
    _controller = TextEditingController();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const EnatAppBar(),
      body: _buildBody(context),
    );
  }

  ListView _buildBody(BuildContext context) {
    return ListView(
      children: [
        ImageJumbotron(imgLocation: imgLocation),
        _buildMilkTypeSwitch(),
        _buildDescriptionText(),
        Header3(title: title),
        _buildForm(context),
        _buildResultDisplay(),
      ],
    );
  }

  Padding _buildResultDisplay() {
    return Padding(
        padding: const EdgeInsets.all(16.0),
        child: EnatParagraph(
          body: _message,
        ));
  }

  Form _buildForm(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildWeightTextInput(),
          _buildAgeSelector(context),
          _buildSubmitButton(),
        ],
      ),
    );
  }

  Center _buildSubmitButton() {
    return Center(
      child: EnatButton(
        onPressed: _calculateChildFood,
        text: 'አስብ',
      ),
    );
  }

  Padding _buildAgeSelector(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Row(
        children: [
          const EnatParagraph(body: 'የልጅሽ ዕድሜ'),
          DropdownButton<int>(
            hint: const Text('የልጅሽ ዕድሜ'),
            value: _selectedAgeGroup,
            icon: const Icon(Icons.arrow_drop_down),
            elevation: 16,
            style: Theme.of(context).textTheme.bodyText1,
            underline: Container(height: 2),
            onChanged: (int? newValue) {
              setState(() {
                _selectedAgeGroup = newValue!;
              });
            },
            items: List.generate(ageGroup.length, (index) => index)
                .map<DropdownMenuItem<int>>((int index) {
              return DropdownMenuItem<int>(
                value: index,
                child: Text(ageGroup[index]),
              );
            }).toList(),
          ),
        ],
      ),
    );
  }

  Padding _buildWeightTextInput() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: TextField(
        style: const TextStyle(color: Colors.grey),
        expands: false,
        keyboardType: const TextInputType.numberWithOptions(),
        decoration: InputDecoration(
          hintText: 'ለምሳሌ 4.35',
          labelText: 'ልጅሽ ክብደት (በኪግ) ',
          errorStyle: const TextStyle(fontSize: 12),
          errorText: _errorMessage,
        ),
        controller: _controller,
        onSubmitted: (String value) {
          setState(() {
            try {
              _childWeight = double.parse(value);
            } catch (e) {
              setState(() {
                _errorMessage = 'ትክክለኛ የሆነውን የልጅሽን የክብደት መጠን ማስገባት አለብሽ፡፡';
              });
            }
          });
        },
      ),
    );
  }

  EnatSwitch _buildMilkTypeSwitch() {
    return EnatSwitch(
        leftLabel: 'የጡት ወተት',
        rightLabel: 'የዱቄት ወተት',
        value: _isPowderMilk,
        onChange: (value) {
          setState(() {
            _isPowderMilk = value;
          });
        });
  }

  _calculateChildFood() {
    if (_controller.text.isEmpty) {
      setState(() {
        _errorMessage = 'ያስገባሽው ክብደት መጠን የተሳሳተ ነው፡፡';
        _message = '';
      });
      return;
    }

    setState(() {
      _childWeight = double.parse(_controller.text);
    });

    if (_childWeight < 0 || _childWeight > 45) {
      _errorMessage = 'የእርግዝና ሳምንት የሚቀበለው ከ0 እስከ 45 ድረስ ያሉ ቁጥሮችን ብቻ ነው፡፡';
      _message = '';
      return;
    }

    setState(() {
      _errorMessage = null;
    });

    if (_isPowderMilk) {
      _getPowderMilkDiet();
    } else {
      _getBreastMilkDiet();
    }
  }

  void _getBreastMilkDiet() {
    if (_childWeight >= 2.5) {
      int intake = (_childWeight * 150).floor();
      setState(() {
        _message =
            '$intake ሚሊ በየቀኑ ለልጅሽ ማጠጣት አለብሽ፡፡ ይህ ማለት በአማካይ ${(intake / 8).floor()}ሚሊ ለ8 የተለያዩ ጊዜያት ማጥባት አለብሽ ማለት ነው፡፡';
      });
    } else {
      int intake = (_childWeight * 200).floor() - 60;
      _message =
          '$intakeሚሊ በየቀኑ ለልጅሽ ማጠጣት አለብሽ፡፡ መጀመር 60 ሚ.ሊ ማጥባት ቀጥሎም፤ በአማካይ ${(intake / 7).floor()}ሚሊ 7 የተለያዩ ጊዜያት ማጥባት አለብሽ ማለት ነው፡፡';
    }
  }

  void _getPowderMilkDiet() {
    switch (_selectedAgeGroup) {
      case 0:
        {
          setState(() {
            var lowerIntake = (_childWeight * 30).floor();
            var upperIntake = (_childWeight * 60).floor();
            _message = 'በተለያዩ ምክንያቶች የዱቄት ወተት መስጠት አስፈላጊ ከሆነ የሚከትለውን ተመልከቺ። '
                'ከ$lowerIntakeሚሊ እስከ $upperIntakeሚሊ በየቀኑ ለልጅሽ ማጠጣት አለብሽ፡፡ '
                'ይህ ማለት በአማካይ ከ${(lowerIntake / 8).floor()}ሚሊ እስከ ${(upperIntake / 8).floor()}ሚሊ 8 የተለያዩ ጊዜያት ማጥባት አለብሽ ማለት ነው፡፡ '
                'ይህ  አማካይ መጠን ነው  ሃኪምሽን ማማከር ይፈልጋል።';
          });
          break;
        }
      case 1:
        {
          var intake = (_childWeight * 150).floor();
          setState(() {
            _message = 'በተለያዩ ምክንያቶች የዱቄት ወተት መስጠት አስፈላጊ ከሆነ የሚከትለውን ተመልከቺ። '
                '$intakeሚሊ በየቀኑ ለልጅሽ ማጠጣት አለብሽ፡፡ '
                'ይህ ማለት በአማካይ ${(intake / 8).floor()}ሚሊ 8 የተለያዩ ጊዜያት ማጥባት አለብሽ ማለት ነው፡፡ '
                'ይህ  አማካይ መጠን ነው  ሃኪምሽን ማማከር ይፈልጋል።';
          });
          break;
        }
      case 2:
        {
          var intake = (_childWeight * 120).floor();
          setState(() {
            _message = 'በተለያዩ ምክንያቶች የዱቄት ወተት መስጠት አስፈላጊ ከሆነ የሚከትለውን ተመልከቺ። '
                '$intakeሚሊ በየቀኑ ለልጅሽ ማጠጣት አለብሽ፡፡ '
                'ይህ ማለት በአማካይ ${(intake / 8).floor()}ሚሊ 8 የተለያዩ ጊዜያት ማጥባት አለብሽ ማለት ነው፡፡ '
                'ይህ  አማካይ መጠን ነው  ሃኪምሽን ማማከር ይፈልጋል።';
          });
          break;
        }
      case 3:
        {
          var intake = (_childWeight * 100).floor();
          setState(() {
            _message = 'በተለያዩ ምክንያቶች የዱቄት ወተት መስጠት አስፈላጊ ከሆነ የሚከትለውን ተመልከቺ። '
                '$intakeሚሊ በየቀኑ ለልጅሽ ማጠጣት አለብሽ፡፡ '
                'ይህ ማለት በአማካይ ${(intake / 8).floor()}ሚሊ 8 የተለያዩ ጊዜያት ማጥባት አለብሽ ማለት ነው፡፡ '
                'ይህ  አማካይ መጠን ነው  ሃኪምሽን ማማከር ይፈልጋል።';
          });
          break;
        }
    }
  }

  EnatParagraph _buildDescriptionText() {
    return _isPowderMilk
        ? const EnatParagraph(body: 'በቀን ምን ያህል የዱቄት ወተት ለልጅሽ መስጠት እንደሚገባ ያሰላል')
        : const EnatParagraph(body: 'በቀን ምን ያህል የጡት ወተት ለልጅሽ መስጠት እንደሚገባ ያሰላል');
  }
}
